/**
 * @file
 * @brief This file contains definitions for data structures used in RAWState 
 * and basic I/O methods for reading and storing trajectory information read from external files
 *
 * @author MSc. Eng. Kamil Rybacki
 * 
 * @date 21/04/2019
 * 
 */

#ifndef RAWSTATE_H
#define RAWSTATE_H

#include <fstream>
#include <vector>

// Helper functions


// Break points for debugging 

#define BP bp();
#define SBP(str) sbp((str));

/** @name Debugging breakpoints 
 * These functions print out to the console either a number (which increments with every use of the function) - bp or custom text - sbp(). These are purely for debugging purposes for lazy people who don't want to write the whole cout << etc. etc. line. Very lazy stuff. Each of these functions have their corresponding macro - BP and SBP. BP eliminates need for parentheses (additional layer of laziness). SBP is for consistency sake here. 
 */
void bp();
void sbp(std::string text);
/**@{*/

/** @name Misc functions 
 * These functions are some basic operations used during development of RawState i.e. simple math functions, regex replacement of a substring within a string etc. They are here to avoid using external libraries and keep RawState self-contained. 
 */
/**@{*/

/**
 * This function replaces a part of an input string, denoted here as key, with user-defined substitute
 * @param[in] input Input string to be modified
 * @param[in] key Pattern within an input string to replace
 * @param[in] substitute String by which found patterns are to be substituted
 */
void replaceSubstring(std::string& input, const std::string& key, const std::string& substitute); 
/**
 * This function is a recursive implementation of a factorial
 * @param[in] n Argument for a factorial function
 * @returns Factorial of input number
 */
int factorial(int n);
/**
 * This function is a recursive implementation of a inverse factorial
 * @param[in] n Argument for a inverse factorial function
 * @returns Inverse factorial of input number
 */
int inverse_factorial(int factorial);

/**@}*/

namespace RawState
{
	// Setter and getter for thermodynamic state timestep

  /** @name Settings for LAMMPS dumpfile and thermo logs parser 
   * These functions control the starting point and the data section interval variables
   * used during parsing of the external datafiles
   */
  /**@{*/
  /**
   * This function sets the timestep data section from which the file is read
   * @param state_timestep Starting timestep
   */
	void setStateTimestep(int state_timestep);
  /**
   * This function retrieves the timestep data section number from which the file is read
   */
	int getStateTimestep();
  /**
   * This function sets how many data sections should be skipped between each subsequent timestep reads 
   * @param state_timestep_delta Interval between parsed timesteps
   */
	void setStateTimestepDelta(int state_timestep_delta);
  /**
   * This function retrieves how many data sections are skipped between each subsequent timestep reads 
   */
	int getStateTimestepDelta();
  /**@}*/

  /** @name Basic analysis methods 
   * These functions are used during preliminary analysis of extracted particle data
   */
  /**@{*/
  /**
   * This function checks PBC in triclinic simulation box for a pair of atoms and checks if they lie within the cutoff radius
   * @param[in] i_position Absolute coordinates of i-th atom in 1D array
   * @param[in] j_position Absolute coordinates of j-th atom in 1D array
   * @param[in] norm_a Norm of simulation box basic vector a
   * @param[in] norm_b Norm of simulation box basic vector b
   * @param[in] norm_c Norm of simulation box basic vector c
   * @param[in] r_cutoff Cutoff radius
   * @param[in] is_periodic 1D array containing bool variables for PBC along three general directions e.g. (TRUE,TRUE,FALSE) for x,y,z respectively
   * @param[out] bond_length Distance between i-th and j-th atom 
   */
	bool checkBoundaryConditions(double *i_position, double *j_position, double norm_a, double norm_b, double norm_c, double r_cutoff, bool *is_periodic, double &bond_length);
  /**@}*/

  /**
   * @brief Structure containing information about geometry read from external file, used in methods specified namespaces such as RawStateModify namespace and stored as a part of RawState::System structure.
   *
   */
  struct ParseInfo
  {
    int _n_atoms; /**< @brief Number of particles inside the stored structure*/		
    
    int _n_types; /**< @brief Number of unique elements inside a structure*/
    std::string _type_labels; /**< @brief Labels of elements inside a structure*/
    
    std::string _description; /**< @brief Short description of the structure*/

    int *_stechiometry; /**< @brief 1D array containing stechiometric coefficients of each element inside the structure*/
    std::string _stechiometry_desc; /**< @brief Stechiometry of imported compound saved in \f$A_xB_yC_z\ldots\f$ format*/
    std::string _filepath; /**< @brief Path to the file from which data has been recently read*/

  /**
   * @brief Function which initializes memory for the 1D RawState::ParseInfo::_stechiometry array
   */

    void _initializeStechiometry();

    ParseInfo();
    ~ParseInfo();
  };
  /**
   * @brief Structure representing a single particle
   *
   * This data structure contains information about an atom read from external data file.
   * As a bare minimum it contains a numerical ID for a chosen particle (used for example to
   * group atoms into different subsets like elements), its position vector stored both as
   * by use of absolute Cartesian coordinates and partial coordinates relative to simulation box
   * basic vectors e.g. [0.5,0.5,0.25] and its atomic mass. The parser for simulation output files
   * included within various types of RAWState*Tools namespaces (e.g. RAWStateLAMMPSTools) automatically 
   * detects whether the file contains additional information about velocities and forces and allocates
   * extra memory within each Atom structure to contain such data. This is done through _create* functions.
   * Additionally, a coordination number can be extracted from Verlet list generated by this library and
   * stored in _coordination_number variable, if such list is to be deleted to save memory
   * reserved for other data such as bond lengths etc.
   *
   */
	struct Atom
	{

		bool _is_present; /**< @brief Tells if and atom should be physically present in simulation box */
		int _type_id; /**< @brief A numeric tag for diffrentiating types of atoms e.g. different elements */
    
    /** @brief Absolute and partial coordinates of an atom. 
     *
     * 1D array containing 6 elements - 3 for absolute Cartesian coordinates and the other 3 for partial coordinates w.r.t. simulation box basic vectors
     *
     */
		
    double *_position; 		
    double *_velocity; /**< @brief 1D array containing velocity vector for a given atom*/
		double *_force; /**< @brief 1D array containing total force vector for a given atom*/
		double _mass; /**< @brief Atom mass*/
    int _coordination_number; /**< @brief Coordination number for a given atom*/
		
		Atom();
		~Atom();

    /** @name Setters 
     * Setters for various Atom members  
     */
    /**@{*/
    /**
     *
     * @brief Setter for 'physical presence' tag Atom::_is_present
     * @param[in] is_present Boolean variable for which TRUE means that particle is present in simulation box
     *
     */
		void _setExistance(bool is_present);
    /**
     *
     * @brief Setter for atomic mass Atom::_mass
     * @param[in] mass Mass of the particle
     *
     */
		void _setMass(double mass);
    /**
     *
     * @brief Setter for numerical ID Atom::_type_id
     * @param[in] type_id Numerical tag assigned to the particle 
     * 
     */
		void _setTypeId(int type_id);
    /**@}*/

    /** @name _create* functions 
     * Memory alocators for arrays containing microstate of the system
     */
    /**@{*/
    /**
     *
     * @brief Memory allocator for 1D array containing position of the particle (see Atom::_position for more info)
     * 
     */
		void _createPositionsTable();
    /**
     *
     * @brief Memory allocator for 1D array containing velocity vector of the particle 
     * 
     */
		void _createVelocitiesTable();
    /**
     *
     * @brief Memory allocator for 1D array containing vector of total force acting on the particle 
     * 
     */
		void _createForcesTable();
    /**@}*/
	};
  /**
   * @brief Structure containing Verlet neighbours list
   *
   * This structure contains Verlet list for a system containing _n_atoms 
   * number of atoms, represented using three pointer arrays: @n
   * 1) 1D array containing number of neighbours for each atom - _n_neighbours @n
   * 2) 2D array containing a 1D array for each atom in the system, filled with numerical ID<SUP>1</SUP> of its neighbours - _neighbours_ids @n
   * 3) An array similar to 2), but containing bond lengths instead of aforementioned IDs - _bond_lenghts
   *
   * This data structure is first initialized using _prepareNeighbourList function to allocate required memory.
   * This action should be performed by other methods after initialization of NeighbourList structure
   * and setting the number of particles present in simulation box by _setNumberOfAtoms function!
   * The pseudo-code below can be used as a general protocol of preparing and building Verlet lists:
   * 
   * \code{.cpp}
   *
   * // PREPARING DATA CONTAINERS
   *
   * int n_atoms = 1000;
   * RawState::NeighbourList *example = new NeighbourList;
   * example->_setNumberOfAtoms(n_atoms);
   * example->_prepareNeighboursList;
   * RawState::Atom *atoms_from_file = new Atom [n_atoms];  
   * 
   * // GATHERING DATA //
   *
   * HERE YOU HAVE TO READ DATA FROM EXTERNAL FILE AND STORE LIST OF ATOMS 
   * IN THE ARRAY PREPARED ABOVE (atoms_from_file). ADDITIONALLY YOU NEED 2D ARRAY CONTAINING SIMULATION 
   * BOX BASIC VECTORS (basic_vectors as System::_basis_vectors) AND INFO ABOUT IMPOSED PERIODIC BOUNDARY 
   * CONDITIONS (pbc_info as System::_is_periodic). ALL THIS CAN BE OBTAINED USING RawState::System METHODS!!
   * 
   * // BUILDING VERLET LIST
   *
   * example->_buildNeighboursList(basic_vectors,atoms_from_file, pbc_info);
   * \endcode
   *
   * (1) - numerical ID here has the meaning of the particle's ordinal number within System::_particles array
   *
   */
	struct NeighbourList
	{
		int _max_neighbours; /**< Maximum number of allowed neighbours */
		double _r_cutoff; /**< Cutoff radius used during Verlet list generation */

		// Verlet list tables
		
		int _n_atoms; /**< Number of atoms within the system */
		int *_n_neighbours; /**< 1D array containing number of each atom's neighbours */
    /** 
     *
     * 2D array containing numerical IDs of each atom's neighbours i.e. array of 1D arrays,
     * with each of them being of length equal to the value read from NeighbourList::_n_neighbours
     * table for a given atom, for example:
     *
     * \code{.cpp}
     * NeighbourList *list = new NeighbourList;
     *
     * // PREPRARATION AND BUILDING OF LIST ..
     * 
     * int number_of_neighbours;
     * for(int atom = 0; atom < n_atoms; atom++)
     * {
     *    number_of_neighbours = list->_n_neighbours[atom]; // Here we read # of neighbours for one of the atoms
     *    for(int atom_neighbour = 0; atom_neighbour < number_of_neighbours; atom_neighbour++)
     *    {
     *      cout << list->_neighbours_ids[atom][atom_neighbour] << " ";
     *    }
     *    cout << "\n";
     * }
     * \endcode
     *
     * This will print out numerical IDs of all neighbours found for each of the atoms present in the simulation box.
     * 
     */
		int **_neighbours_ids; 
		double **_bond_lengths; /**< Data array similar to NeighbourList::_neighbours_ids, but containing calculated bond lengths instead of numerical IDs */

		NeighbourList();
		~NeighbourList();
		
    /** @name Setters 
     * Setters for various NeighbourList members  
     */
    /**@{*/
    /**
     *
     * @brief Setter for number of atoms in the system
     * @param[in] n_atoms Number of atoms to be set for Verlet list
     *
     */
		void _setNumberOfAtoms(int n_atoms);
    /**
     *
     * @brief Setter for maximum number of neighbours allowed for each atom
     * @param[in] max_neighbours Maximum amount of neighbour per one atom to be set
     *
     */
		void _setMaxNeighbours(int max_neighbours);
    /**
     *
     * @brief Setter for cutoff interatomic distance used while generating the Verlet list
     * @param[in] r_cutoff Cutoff radius to be set
     *
     */
		void _setCutoff(double r_cutoff);
    /**@}*/
		
    /** @name Preparation and construction of list contents 
     * These functions allocate the memory needed by a Verlet list to be counstructed for
     * number of atoms set previously via NeighbourList::_setNumberOfAtoms function and stored in NeighbourList::_n_atoms variable
     */
    /**@{*/
    /**
     *
     * @brief Function which allocates the memory, based on the number of atoms stored in NeighbourList::_n_atoms 
     *
     */
		void _prepareNeighboursList();
    /**
     *
     * @brief Function for building Verlet list. It operates on a list of particles passed as 1D array
     * of RawState::Atom structures, from which two particles ( i-th and j-th atom ) are chosen. Next,
     * their partial coordinates are used together with the basic vector's norms ( passed here as 2D 
     * pointer array corresponding to System::_basis_vectors table ) and info about PBC set 
     * for the simulation box. As a result, the interatomic distance i.e. bond length is calculated
     * and checked against the following condition: bond_length < NeighbourList::_r_cutoff. If j-th atom
     * satisfies this condition, then it is added to the list of neighbours for i-th atom.
     *
     * @param[in] box_vectors Matrix composed of simulation box basic vectors (3x3)
     * @param[in] atoms List of particles for which the Verlet list is to be constructed
     * @param[in] is_periodic Info about PBC set for the simulation box (see System::_periodic) 
     *
     */
		void _buildNeighboursList(double **box_vectors, Atom *atoms, bool *is_periodic);
    /**@}*/

	};
  /**
   * @brief Structure containing system info read directly from datafile (microstate)
   *
   * The most general data structure within RawState library which contains information about
   * a microstate read from external datafile for one of the simulation timesteps. This information,
   * in its most basic state, is composed of the list of particles present in the simulation box,
   * simulation box basic vectors, its point of origin and form of PBC imposed onto the system.
   *
   * This information is read using System::_readNextTimestep function, which automatically remembers
   * which data section has been read from the file during its last usage. The starting point and timestep 
   * interval between each of the readings can be changed via RawState::setStateTimestep and RawState::setStateTimestepDelta 
   * functions respectively. For example, using the following pseudo-code for LAMMPS simulation output:
   * 
   * \code{.cpp}
   * System *example = new System;
   * 
   * // We're starting the reading from the beginning (0-th timestep) and we will extract data from 
   * // each 10th data section included in the file e.g. if LAMMPS wrote the dump file every 100 timesteps
   * // during 10 000 timestep simulation run, this means that the file initially contains 100 data sections.
   * // If we set RawState::_state_timeste_delta to 10, this means that data about sections 0,10,20,...,100
   * // will be read during subsequent calls of System::_readNextTimestep function
   *
   * RawState::setStateTimestep(0);
   * RawState::setStateTimestepDelta(10);
   
   * std::string filename = "trajectory.lammpstrj"; // Trajectory file generated by LAMMPS
   * int number_of_readings = RawStateLammpsTools::getNumberOfReadings(filename); 
   * 
   * for(int reading = 0; reading < number_of_readings; reading++)
   * {
   *    example->_readNextTimestep(filename);
   *    // Calculations, analysis, writing etc. ...
   * }
   * \endcode
   *
   * , user is able to perform some set of operations on the microstate for a given timestep e.g.
   * creating Verlet list and carrying out algorithms such as CNA. The RawStateLAMMPSTools::getNumberOfReadings
   * function automatically calculates the number of times the System::_readNextTimestep function is to
   * be performed on a given file. @n <B>IT SHOULD ALWAYS BE CARRIED OUT WITH RawState::_system_timestep AND
   * RawState::_system_timestep_delta SETTERS IN THE ORDER SHOWN ABOVE BEFORE READING OF THE FILE!!</B> 
   *
   * Additionally, NeighbourList for a given timestep can be constructed via System::_createNeighboursList function
   * or manual wrapping of atoms to the simulation box can be performed for output files generated for example by LAMMPS.
   *
   */
	struct System
	{	
		// Information about atoms 

		int _n_atoms; /**< Number of atoms within the system */
		Atom *_particles; /**< List of Atom structures, each corresponding to the particle present within the system */
		NeighbourList *_neighbours; /**< Verlet list generated on the basis of extracted particle data */
		double _r_cutoff; /**< Cutoff radius used when considering particle interactions within the system */

		bool _toggle_velocity; /**< Boolean variable used for signifying if the datafile contains additional info about particle velocities */
		bool _toggle_force; /**< Similar to System::_toggle_velocity, but for total interatomic forces */
    /**
     * Boolean variable used to toggle normalization of atomic coordinates. It is automatically set to True if one of the parsers e.g. 
     * the RawState::System::_readNextLAMMPSTimestep() function detects one of the scaled coordinates keywords (xs,ys,zs) in the header of
     * one of the timesteps recorded in the LAMMPS dump file.
     *
     * It should be noted that <B> if at least one atomic coordinate is scaled by a simulation program, then all of them must also be
     * already provided in this form </B>.
     */
    bool _normalize; 
    /**
     *
     * Structure containing information read by the library while processing a geometry file, which contains data about: number of atoms in the system, number of unique elements, description contained in a string variable and other values, helpful during future modifications of the structure. 
     * 
     */
    ParseInfo *_parse_data;
    /**
     * String variable to store custom text 
     */
    std::string _misc_description;

		// Information about box shape and size
		/**
     *
     * 3x3 matrix containing information about triclinic simulation box bounds, 
     * based by notation used in LAMMPS software. Its columns are composed of the following data:@n
     * 1) Lower bounds (x<SUB>min</SUB>,y<SUB>min</SUB>,z<SUB>min</SUB>) of orthogonal box enclosing the simulation box,@n
     * 2) Upper bounds (x<SUB>max</SUB>,y<SUB>max</SUB>,z<SUB>max</SUB>) of orthogonal box enclosing the simulation box,@n
     * 3) Tilt factors (xy,xz,yz) of each of the triclinic box faces.
     *
     * For more detalied information, it is highly recommended to read the snippet from LAMMPS documentation about
     * triclinic simulation boxes: <a href="https://lammps.sandia.gov/doc/Howto_triclinic.html">Triclinic (non-orthogonal) simulation boxes</a>.
     * This convention in RAWState is used for several reasons. The simple one is that this library has been first used for working with LAMMPS simulation
     * output files. So from the get-go, it was intended mainly for use in conjunction with this software. However, data from other simulation suites can
     * be either easily converted to this format knowing the point of origin of the simulation box, box lengths and its tilt factors as specified in the
     * documentaton linked above. 
     *
     */
		double **_box_data; 
		/**
     *
     * 3x3 matrix containing information about triclinic simulation basis vectors, calculated from data
     * contained in System::_box_data. Again, please refer to the snippet from LAMMPS documentation for
     * a very detailed description of non-orthogonal simulation boxes: <a href="https://lammps.sandia.gov/doc/Howto_triclinic.html">Triclinic (non-orthogonal) simulation boxes</a>.
     * This array is used for example during computations of interatomic distances to enforce PBC via minimum image convention.
     *
     */
		double **_basis_vectors;
		double *_point_of_origin; /**< Coordinates of simulation box' point of origin */
    /**
     * Boolean 1D array containing settings of Periodic Boundary Conditions (PBC) in directions 
     * of each of simulation box basis vectors (<B>a</B>,<B>b</B> and <B>c</B>). 
     * If set to TRUE, then PBC are enforced along chosen box edge.
     *
     * Note that for orthogonal simulation boxes, vectors <B>a</B>, <B>b</B> and <B>c</B> form an orthogonal
     * basis, aligned with Cartesian general directions. Hence in this case, their directions correspond to 
     * x, y and z axes respectively.
     *
     */
		bool *_periodic; 
		int _timestep_label; /**< Timestep number read from the data section within external file */

		System();
		~System();

    /** @name Setters 
     * Setters for various System members  
     */
    /**@{*/
    /**
     *
     * @brief Setter for simulation box data matrix, read from external files
     * @param[in] box_data 3x3 simulation box matrix, passed according to rules specified in System::_box_data
     *
     */
    void _setBoxVertices(double **box_data);
    /**
     *
     * @brief Setter for cutoff interatomic distance calculations
     * @param[in] r_cutoff Cutoff radius to be set
     *
     */
		void _setCutoff(double r_cutoff);
    /**
     *
     * @brief Setter for number of atoms in the system
     * @param[in] n_atoms Number of atoms to be set for Verlet list
     *
     */
		void _setNAtoms(int n_atoms);
    /**
     *
     * @brief Setter for PBC settingd
     * @param[in] p{vector} If set to TRUE (1), the PBC are enforced along corresponding basis {vector} (<B>a</B>, <B>b</B> or <B>c</B>)
     *
     */
		void _setPeriodicity(bool pa, bool pb, bool pc);
    /**@}*/
		
    /** @name Microstate storage and pre-analysis 
     * Functions which handle allocation of memory used for storing particle information extracted from 
     * datafile and for Verlet list to be optionally constructed on the basis of atoms info. In this subset
     * of functions, methods for atom wrapping and neighbour list generation are included as set of
     * preliminary analysis methods, used in a wide variety of computational methods.
     */
    /**@{*/
    /**
     *
     * @brief Function for allocating memory for list of particles i.e. 1D array of Atom structures 
     *
     */
		void _createParticlesTable();
    /**
     *
     * @brief Function for allocating memory for list of particles i.e. 1D array of Atom structures with instant allocation of their positions tables 
     *
     */
    void _createParticlesTableWithPositions();
    /**
     *
     * @brief This which allocates memory for RawState::ParseData data structure when needed
     *
     */
		void _createParseData();
    /**
     *
     * @brief This function calculates simulation box basis vectors
     * 
     * This function calculates simulation box basis vectors using data previously read and stored in System::_box_data array.
     * The way in which elements of this matrix are converted to basis vectors is carried out with accordance to method explained
     * in LAMMPS documentation: <a href="https://lammps.sandia.gov/doc/Howto_triclinic.html">Triclinic (non-orthogonal) simulation boxes</a>.
     *
     */
		void _calculateBasisVectors();
    /**
     *
     * @brief This function normalized atomic coordinates to the dimensions of simulation box
     * 
     * The normalization of atomic coordinates is carried out using information about simulation box shape stored in
     * RawStat::System::_basis_vectors array.
     *
     */
    void _normalizeAtoms();
    /**
     *
     * @brief Function which loops over stores list of particles 
     * and wraps them into the simulation box if the wrapping wasn't performed by simulation software
     * @param[in] normalize Tells wheather to normalize atomic coordinates w.r.t. simulation box 
     *
     */
		void _wrapAtoms(bool normalize);
    /**
     *
     * @brief This function checks the partial coordinates of atoms to enable future replications of an elementary cell if the input geometry was of the single-cell type 
     * 
     */
		void _enablePeriodicity();
    /**
     *
     * @brief Method for creating Verlet neighbours list based on previously stored particle data
     * @param[in] r_cutoff Cutoff radius used during list construction 
     * @param[in] max_neighbours Maximum amount of neighbours allowed for single atom 
     * @param[in] normalize Tells wheather to normalize atomic coordinates w.r.t. simulation box 
     *
     */
    void _createNeighboursList(double r_cutoff, int max_neighbours, bool normalize);
    /**@}*/
    
    /** @name Parsers 
     *  Functions used for reading subsequent data sections contained in external datafiles  
     */
    /**@{*/
    /**
     *
     * @brief Function which reads next data section based on values kept in RawState::_state_timestep
     * and RawState::_state_timestep_delta environment variables. Any preceeding uses of this function
     * are remembered though modification of those variables after each reading. In other words, if 
     * initial timestep is set to 0 and timestep delta is set to 10, then the first call of this function
     * will read the data section number 0 in the file. If the function is called again, then data section
     * number 10 is read automatically.
     *
     * To reset or modify this internal counter, RawState::setStateTimestep function has to be used to set the
     * parser to the desired data section in the file.
     * 
     * @param[in] filename String variable containing filepath to LAMMPS trajectory file (dumpfile) 
     *
     */
		bool _readNextLAMMPSTimestep(std::string filename);
    /**@}*/
	};

	/* TO BE IMPLEMENTED LATER !!!
	struct ThermoData
	{

	}
	struct RadialDistribution
	{

	}
	struct SpatialDisplacements
	{

	}
	struct Coordination
	{

	}
	struct ThermoState
	{
		// Microstate is the spatial configuration of particles in 3D space
		// Macrostate is a statistical ensemble described by thermodynamic state
		// i.e. temperature, volume, pressure tensor etc.
		// Macrostate results from microstate and the other way around

		// Current state of the system

		State *_microstate;
		ThermoData *_macrostate;

		// Initial state of the system

		State *_initial_microstate;
		ThermoData *_initial_macrostate;

		// Additional information about the system's current state:

		RadialDistribution *_rdf; // Radial Distribution Function
		SpatialDisplacements *_displacements; // Displacements in relation to initial state
		Cooridination *_coordinationNumbers; // Close range coorindation based on Verlet list

		Thermostate();
		~Thermostate();
	}
	*/
	
	// Printers

  /** @name Particle info printers 
   * Mostly utility functions used for printing stored particle data (useful while debugging)
   */
  /**@{*/
    /**
     * @brief Function for printing out Cartesian coordinates of all particles in the system 
     * @param[in] input RawState::System structure used previously during extraction of microstate data
     */
	void printParticlePositions(System *&input);
    /**
     * @brief Function for printing out partial coordinates of all particles in the system 
     * @param[in] input RawState::System structure used previously during extraction of microstate data
     */
  void printParticleRelativePositions(RawState::System *&input);
  /**@}*/
}
namespace RawStateGeometry
{
  /** @name RAWState Tools for reading and modifying data stored in external geometry files 
   * Functions used for reading external geometry files, extracting data and then writing its processed form to output files
   */
  /**@{*/
  /**
   * @brief Function which reads coordinates of particles and basic vectors of an elementary cell included in POSCAR files 
   *
   *
   * @param[in] filename Path to datafile to be processed
   * @param[in] input RawState::System structure to which the parsed data will be written
   */
	void readPOSCAR(std::string filename, RawState::System *&input);
  /**
   * @brief Function which reads coordinates of particles and basic vectors of an elementary cell included in XYZ files 
   *
   *
   * @param[in] filename Path to datafile to be processed
   * @param[in] input RawState::System structure to which the parsed data will be written
   */
	void readXYZ(std::string filename, RawState::System *&input);
  /**@{*/
  /**
   * @brief Function used to determine stechiometry of a compound read from external geometry file 
   *
   *
   * @param[in] filename Path to datafile to be processed
   * @param[in] input RawState::System structure to which the parsed data will be written
   */
	void determineStechiometry(RawState::System *&input);
  /**@{*/
  /**
   * @brief Function used to assign labels of chemical elements to atomic types read from external file 
   *
   *
   * @param[in] input RawState::System structure to which the parsed data will be written
   * @param[in] elements String variable containing labels of elements to be used while creating later structures
   */
	void assignElements(RawState::System *&input, std::string elements);
  /**@{*/
  /**
   * @brief This function creates a hypothetical elementary cells of symmetry specified in geometry data stored in RawState::System structure. This cell is composed of elements specified in std::string variable passed to the function. The amount of RawState::system structures stored in the std::vector output is determined based on the type of the input geometry data i.e. if its a heteroatomic cell of \f$A_xB_y\f$ stechiometry then 2 structures will be outputted for a case when x is not equal to y - \f$A_xB_y\f$ and \f$B_xA_y\f$. It should be noted that this function makes use of the Standard C++11 Library implementation of vector object, which generally is avoided in the RawState Library. This exception is made in this case for simplicity sake - to quickly and automatically generate an unknown amount of hypothetical structures and then go over them in the later stages of the program.
   *
   *
   * @param[in] input  RawState::System structure containing stored geometry data
   * @param[in] output Pointer array of RawState::System structures containing generated configurations 
   * @returns Number of generated structures
   */
  int createHypotheticalStructures(RawState::System *&input, RawState::System **&output);
  /**@}*/
}
namespace RawStateLAMMPSTools
{
  /** @name RAWState Tools for LAMMPS 
   * Functions used for reading LAMMPS trajectory, extracting data and then writing its processed form to output files
   * when the input files are generated by LAMMPS simulation software
   */
  /**@{*/
  /**
   * @brief Function which automatically calculates number of readings to be done via System::_readNextTimestep method
   *
   * This function operates on the following basis. At first, the total number of lines present in the LAMMPS dump file
   * is counted. At the same time, number of atoms is read from one of the data sections to calculate number of lines
   * belonging to a single timestep as: NUMBER_OF_PARTICLES + 9. Then, total number of lines is divided by this amount, which
   * results in total number of data sections (i.e. recorded timesteps) present in the file. From this value, _state_timestep
   * variable is deducted to account for the shift of the data section from which the file should be read. For example, if
   * _state_timestep equals to 0, the the file is read beginning from first data section. However, if it is changed to 10, then
   * the reading will commence from the 10th data section, counting from beginning of the file. After this substraction,
   * the remaining number is divided by _state_timestep_delta. Denoting the value of this parameter as <I>n</I>, then every
   * <I>n</I>-th data section will be read, instead of each subsequent one. To assure that every one data section is read, the
   * value of _state_timestep_delta should be set to 1.
   *
   * @param[in] filename Path to datafile to be processed
   * @returns Maximum number of System::_readNextTimestep function calls to be used for current settings of _state_timestep
   * and _state_timestep_delta variables and given datafile
   */
	int getNumberOfReadings(std::string filename);

	// Four versions of function to write info about atoms to an external file
	// The second one is overloaded by a string containing custom data to be written
	// which is other than atomic positions i.e. vel - velocity; for - forces
	// The third one accepts an additional string as a custom header.
  // The last one is the combination of the second and third overloaded versions
  
  /**
   * @brief Function for writing <B>BASIC</B> (IDs, types and Cartesan coordinates) particle information to output datafile in XYZ format  
   * @param[in] filename String variable containing path to output file to be written by function 
   * @param[in] input RawState::System data structure which contains previously read and processed particle data 
   */
	void writeTimestepDataToFile(std::string filename, RawState::System *&input);
   
  /**
   * @brief Overloaded of previous function for writing <B>BASIC</B> particle information with custom header  
   * @param[in] filename String variable containing path to output file to be written by function 
   * @param[in] header Custom header in string format to be appended as the first line of data file
   * @param[in] input RawState::System data structure which contains previously read and processed particle data 
   */
	void writeTimestepDataToFile(std::string filename, std::string header, RawState::System *&input);
 
  /**
   * @brief Overloaded version of previous function, for customized writing particle information to output datafile in LAMMPSTRJ format. 
   *
   * In this case, the writing style (write_style) has to be specified using predefined keywords, listed below: @n
   * - part - partial coordinates are used instead of Cartesian ones (simulation box info is also normalized) @n
   * - vel  - particle velocities @n
   * - for  - total forces
   *
   * @param[in] filename String variable containing path to output file to be written by function 
   * @param[in] input RawState::System data structure which contains previously read and processed particle data 
   * @param[in] write_style String containing combination of keywords mentioned above, separated by spaces 
   */
	void writeTimestepDataToFile(std::string filename, RawState::System *&input, std::string write_style);
    
  /**
   * @brief Overloaded version of previous function, for customized writing particle information to output datafile in LAMMPSTRJ format (plus custom header). 
   *
   * In this case, the writing style (write_style) has to be specified using predefined keywords, listed below: @n
   * - part - partial coordinates are used instead of Cartesian ones (simulation box info is also normalized) @n
   * - vel  - particle velocities @n
   * - for  - total forces
   *
   * @param[in] filename String variable containing path to output file to be written by function 
   * @param[in] header Custom header in string format to be appended as the first line of data file
   * @param[in] input RawState::System data structure which contains previously read and processed particle data 
   * @param[in] write_style String containing combination of keywords mentioned above, separated by spaces 
   */
	void writeTimestepDataToFile(std::string filename, std::string header, RawState::System *&input, std::string write_style);
  /**@}*/
}
namespace RawStateLibrary
{
  /** @name RAWState reference data libraries 
   * This namespace contains functions which are digital libraries of atomic masses, radii and other properties which may come
   * useful when creating custom microstructures
   */
  /**@{*/
  /**
   * @brief Returns atomic radius of a chosen element (in Angstroms) 
   * @param[in] atomtype String variable containing label of a chosen element 
   */
  double atomicRadiusList(std::string atomtype);
  /**
   * @brief Returns atomic mass of a chosen element (in units) 
   * @param[in] atomtype String variable containing label of a chosen element 
   */
  double atomicMassList(std::string atomtype);
  /**@}*/
}
#endif /* RAWSTATE_H */
