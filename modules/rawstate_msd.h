/**
 * @file
 * @brief This module contains methods used for calculating mean-squared displacements
 * for a temporial evolution of system's microstate contained within external trajectory files
 * @author MSc. Eng. Kamil Rybacki
 * 
 * This module calculates Mean Squared Displacement using the following equation:
 *
 * \f[
 * MSD = \frac{1}{N}\sum_{i=0}^{N}\left(\mathbf{r}_{i}^{t} - \mathbf{r}_{i}^{0}\right),
 * \f]
 *
 * where \f$\mathbf{r}_{i}^{t}\f$ is the actual position vector of an i-th atom and \f$\mathbf{r}_{i}^{t}\f$ is its reference state.
 * The difference between these values caluclated for each atom are squared and then added together. Position vectors used here are
 * partial coordinates obtained via previous usage of RawState::System::_wrapAtoms function (its automatically invoked by _store* functions). 
 * This sum is averaged over the number of atoms in the system, denoted here as \f$N\f$. 
 * Calculated values of MSD are stored within RawStateMSD::MSD structure.
 *
 * @date 21/04/2019
 * 
 */

#ifndef RAWSTATE_MSD_H
#define RAWSTATE_MSD_H

#include "../rawstate.h"

namespace RawStateMSD
{
  /**
   * @brief Data structure for calculation and storage of Mean Squared Displacements 
   *
   * Within this structure, initial and actual states are kept
   * as 1D pointer arrays of RawState::Atom type of structures, where the latter one
   * is updated during every parsing of external data file.
   *
   */
  struct MSD
  {
    RawState::Atom *_initial_state_atoms; /**< List of Atom structures containing the initial ( or reference ) microstate */
    RawState::Atom *_previous_state_atoms; /**< List of Atom structures containing the previously extracted microstate */
    RawState::Atom *_actual_state_atoms; /**< List of Atom structures containing the most recently extracted microstate */

    double *_actual_box_lengths; /**< Norms of actual simulation box basis vectors used to convert partial coordinate differences to absolute distance units*/
    int _n_atoms; /**< Number of atoms in the system */

    bool *_is_periodic; /**< See RawState::System::_periodic */
    double *_msd; /**< 1D pointer array containing Mean Squared Displacements calculated for actual microstate */ 

    MSD();
    ~MSD();

    /** @name Setters 
     * Setters for various MSD members 
     */
    /**@{*/
    /**
     *
     * @brief Setter function for number of particles in the system
     *
     * @param[in] n_atoms Number of atoms in the system
     *
     */
    void _setNumberOfAtoms(int n_atoms);
    /**@}*/

    /** @name Memory allocators
     * Memory allocators for data structures used during MSD calculations 
     */
    /**@{*/
    /**
     *
     * @brief Memory allocator for initial and actual microstates
     *
     */
    void _createAtomTables();
    /**@}*/
    
    /** @name Microstate storage functions 
     * These functions are used for passing the microstate between RawStateMSD::MSD and RawState::System structures
     */
    /**@{*/
    /**
     *
     * @brief Function which transfers the initial particle data from RawState::System structure
     *
     * Function which transfers the particle data from RawState::System structure, containing data parsed from
     * external data file by a call of RawState::System:_readNextTimestep function. This microstate is to be kept unchanged
     * during MSD calculations as a reference state.
     *
     * @param[in] input RawState::System data structure which contains reference microstate used in MSD calculations i.e. \f$\mathbf{r}^{0}\f$
     * @param[in] if_norm Tells wheather to normalize atomic coordinates w.r.t. simulation box 
     *
     */
    void _storeInitialState(RawState::System *&input, bool if_norm);
    /**
     *
     * @brief Function which transfers the actual particle data from RawState::System structure
     * 
     * Function which transfers the particle data from RawState::System structure, containing data parsed from
     * external data file by the most recent call of RawState::System:_readNextTimestep function. This microstate is treated as actual
     * microstate, used during MSD calculations for its corresponding timestep.
     *
     * @param[in] input RawState::System data structure which contains actual microstate used in MSD calculations i.e. \f$\mathbf{r}^{t}\f$
     * @param[in] if_norm Tells wheather to normalize atomic coordinates w.r.t. simulation box 
     *
     */
    void _storeActualState(RawState::System *&input, bool if_norm);
    /**@}*/
    /** @name MSD struct calculator 
     * Functions used directly for calculating Mean Square Displacements 
     */
    /**@{*/
    /**
     *
     * @brief Function which calculates MSD for data stored previously by _store* functions
     * 
     */
    void _calculateMSD();
    /**@}*/
  };

  /** @name MSD calculators for specific inputs
   * Methods for MSD calculations designed for specific types of input files e.g. LAMMPS trajectory 
   * (.lammpstrj) files.
   */
  /**@{*/
    /**
     *
     * @brief MSD calculation method for LAMMPS trajectory files (dumps)
     * 
     * This function is a method which uses LAMMPS dumpfile parser present in RawState::System structure
     * and calculates instantaneous values of Mean Squared Displacements for subsequent timesteps. The
     * file can be divided time-wise into windows which are portions of time within the scope of simulation
     * within which MSD are calculated independenty. If number of windows is set to 1, then there is no
     * partitioning of data and MSD is calculated for the whole scope of simulation. THe files are written
     * to the folder specified by the user via outpath variable. The name syntax for output files is as
     * follows: ${outpath}/msd_{window_id}.out. Additionally, a file containing the average MSD, calculated
     * across all windows, is created at the same path under the name msd_average.out.
     *
     * @param[in] filename String variable containing the path to input file  
     * @param[in] outpath String variable containing the path to directory where output files are to be created
     * @param[in] start Number of data section within input file from which calculations are to be started (see RawStateLAMMPSTools::getNumberOfReadings for more info)
     * @param[in] n_every Number of data sections to be skipped between subsequent MSD calculations (see RawStateLAMMPSTools::getNumberOfReadings for more info)
     * @param[in] n_windows Number of MSD calculation windows 
     * @param[in] wrap Boolean variable which tells RAWState to wrap atom coordinates to simulation box 
     *
     */
  void calculateMSDForLAMMPSDump(std::string filename, std::string outpath, int start, int n_every, int n_windows);
  /**@{*/
}
#endif /* RAWSTATE_MSD_H */

