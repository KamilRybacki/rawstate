#include <bits/stdc++.h> 
#include "rawstate_math.h"

using namespace std;

void computeRotationMatrix(double **initial_basis, double **target_basis, double **output_matrix)
{

}
int getMinor(double **src, double **dest, int row, int col, int order)
{
    // indicate which col and row is being copied to dest
    int colCount=0,rowCount=0;
 
    for(int i = 0; i < order; i++ )
    {
        if( i != row )
        {
            colCount = 0;
            for(int j = 0; j < order; j++ )
            {
                // when j is not the element
                if( j != col )
                {
                    dest[rowCount][colCount] = src[i][j];
                    colCount++;
                }
            }
            rowCount++;
        }
    }
 
    return 1;
}
double calcDeterminant(double **mat, int order)
{
    if( order == 1 )
        return mat[0][0];
 
    float det = 0;
 
    double **minor;
    minor = new double *[order-1];

    for(int i=0;i<order-1;i++)
        minor[i] = new double [order-1];
 
    for(int i = 0; i < order; i++ )
    {
        getMinor( mat, minor, 0, i , order);
        det += (i%2==1?-1.0:1.0) * mat[0][i] * calcDeterminant(minor,order-1);
    }
 
    // release memory
    for(int i=0;i<order-1;i++)
        delete [] minor[i];
    delete [] minor;
 
    return det;
}
int matrixInversion(double **A, int order, double **Y)
{
    double A_determinant = calcDeterminant(A,order);

    if(A_determinant == 0) return 1;

    double det = 1.0/A_determinant;
 
    double *temp = new double[(order-1)*(order-1)];
    double **minor = new double*[order-1];

    for(int i=0;i<order-1;i++)
        minor[i] = temp+(i*(order-1));
 
    for(int j=0;j<order;j++)
        for(int i=0;i<order;i++)
        {
            getMinor(A,minor,j,i,order);
            Y[i][j] = det*calcDeterminant(minor,order-1);
            if( (i+j)%2 == 1)
                Y[i][j] = -Y[i][j];
        }
 
    delete [] temp;
    delete [] minor;

    return 0;
}
void printTensor(double **A, int order)
{
  for (int i = 0; i < order; ++i)
  {
    for (int j = 0; j < order; ++j)
    {
      cout << A[i][j] << " ";
    }
    cout << "\n";
  } 
}
