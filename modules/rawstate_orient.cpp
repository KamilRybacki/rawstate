#include <iostream>
#include <sstream>
#include <cmath>
#include <climits>
#include <algorithm>
#include <float.h>

#include "rawstate_orient.h"
#include "../rawstate.h"

#define EPSILON 0.00001
#define SHIFT 0.001

using std::cout;

double dotProduct(double *a, double *b, int order)
{
  double product = 0;

  for (int column = 0; column < order; ++column)
    product += a[column] * b[column];

  return product;
}
void crossProduct(double *a, double *b, double *c)
{
    c[0] = ( a[1] * b[2] - b[1] * a[2] );
    c[1] = ( b[0] * a[2] - a[0] * b[2] );
    c[2] = -( a[0] * b[1] - b[0] * a[1] ); 
}
void printVector( double *vector, int order )
{
  for (int column = 0; column < order; ++column)
    cout << vector[column] << " ";
  cout << "\n\n";
}
void printTensor( double **tensor, int rows, int columns)
{

  for (int row = 0; row < rows; ++row)
  {
    for (int column = 0; column < columns; ++column)
      cout << tensor[row][column] << " ";
    cout << "\n";
  }
  cout << "\n\n";
}
inline void printVec3(double *vector){ printVector(vector, 3); }
inline void printMat3by3(double **tensor){ printTensor(tensor, 3, 3); }
int getMinor(double **src, double **dest, int row, int col, int order)
{
    // indicate which col and row is being copied to dest
    int colCount=0,rowCount=0;
 
    for(int i = 0; i < order; i++ )
    {
        if( i != row )
        {
            colCount = 0;
            for(int j = 0; j < order; j++ )
            {
                // when j is not the element
                if( j != col )
                {
                    dest[rowCount][colCount] = src[i][j];
                    colCount++;
                }
            }
            rowCount++;
        }
    }
 
    return 1;
}
double calcDeterminant(double **mat, int order)
{
    if( order == 1 )
        return mat[0][0];
 
    float det = 0;
 
    double **minor;
    minor = new double *[order-1];

    for(int i=0;i<order-1;i++)
        minor[i] = new double [order-1];
 
    for(int i = 0; i < order; i++ )
    {
        getMinor( mat, minor, 0, i , order);
        det += (i%2 == 1?-1.0:1.0) * mat[0][i] * calcDeterminant(minor,order-1);
    }
 
    // release memory
    for(int i=0;i<order-1;i++)
        delete [] minor[i];
    delete [] minor;
 
    return det;
}
int matrixInversion(double **A, int order, double **Y)
{
    double A_determinant = calcDeterminant(A,order);

    if(A_determinant == 0) return 1;

    double det = 1.0/A_determinant;
 
    double *temp = new double[(order-1)*(order-1)];
    double **minor = new double*[order-1];

    for(int i=0;i<order-1;i++)
        minor[i] = temp+(i*(order-1));
 
    for(int j=0;j<order;j++)
        for(int i=0;i<order;i++)
        {
            getMinor(A,minor,j,i,order);
            Y[i][j] = det*calcDeterminant(minor,order-1);
            if( (i+j)%2 == 1)
                Y[i][j] = -Y[i][j];
        }
 
    delete [] temp;
    delete [] minor;

    return 0;
}
void calculateRotationMatrix(double **initial_basis, double **target_basis, double **rotation_matrix)
{
  for (int i = 0; i < 3; i++) 
      for (int j = 0; j < 3; j++) 
      { 
          rotation_matrix[i][j] = 0; 
          for (int k = 0; k < 3; k++) 
              rotation_matrix[i][j] += target_basis[i][k] * initial_basis[k][j]; 
      } 
}
void rotateAtomPositions(RawState::System *input_system, double **rotation_matrix)
{
  double original_positions[3];
  double rotated_positions[3];

  for (int atom = 0; atom < input_system->_n_atoms; atom++)
  {
    for (int component = 0; component < 3; component++)
      original_positions[component] = input_system->_particles[atom]._position[component];

    for (int row = 0; row < 3; ++row)
    {
      double product = dotProduct(rotation_matrix[row],original_positions, 3);

      if(product < EPSILON && product > -EPSILON) rotated_positions[row] = 0.0;
      rotated_positions[row] = product;
    }

    for (int component = 0; component < 3; component++)
      input_system->_particles[atom]._position[component] = rotated_positions[component];
  }
}
void rotateBasisVectors(double **input_basis, double **rotation_matrix, double **output_basis)
{
  for (int vector = 0; vector < 3; vector++)
  {
    for (int row = 0; row < 3; ++row)
    {
      double product = dotProduct(rotation_matrix[row],input_basis[vector], 3);

      if(product < EPSILON && product > -EPSILON) output_basis[vector][row] = 0.0;
      output_basis[vector][row] = product;
    }
  }
}
void generalParallelepipedToTriclinic(double **parabox)
{
  double norms[3];
  double tribox[3][3];

  for (int vector = 0; vector < 3; ++vector)
    norms[vector] = sqrt( parabox[vector][0] * parabox[vector][0] + parabox[vector][1] * parabox[vector][1] + parabox[vector][2] * parabox[vector][2] );

  tribox[0][0] = norms[0];
  tribox[0][1] = 0.0;
  tribox[0][2] = 0.0;
  
  double b_dot_a_norm = 0.0;

  for (int column = 0; column < 3; ++column)
    b_dot_a_norm += parabox[1][column] * ( parabox[0][column] / norms[0] );

  tribox[1][0] = b_dot_a_norm;

  tribox[1][1] = sqrt( norms[1] * norms[1] - tribox[1][0] * tribox[1][0] ); 
  tribox[1][2] = 0.0;

  double c_dot_a_norm = 0.0;

  for (int column = 0; column < 3; ++column)
    c_dot_a_norm += parabox[2][column] * ( parabox[0][column] / norms[0] );

  tribox[2][0] = c_dot_a_norm;

  double b_dot_c = 0.0;

  for (int column = 0; column < 3; ++column)
    b_dot_c += parabox[1][column] * parabox[2][column];

  tribox[2][1] = ( b_dot_c - parabox[1][0] * tribox[2][0] ) / tribox[1][1];
  tribox[2][2] = sqrt( norms[2] * norms[2] - tribox[2][0] * tribox[2][0] - tribox[2][1] * tribox[2][1] );
  
  for (int vector = 0; vector < 3; ++vector)
    for (int column = 0; column < 3; ++column)
      parabox[vector][column] = tribox[vector][column];
}
void RawStateOrient::Generator::_setCrystallographicDirections(int h_1, int k_1, int l_1, int h_2, int k_2, int l_2)
{
  // ( h_1, k_1, l_1 ) -> 0x
  // ( h_2, k_2, l_2 ) -> 0z

  double *x_vec = new double [3];
  double *y_vec = new double [3];
  double *z_vec = new double [3];
  
  x_vec[0] = h_1;
  x_vec[1] = k_1;
  x_vec[2] = l_1;
  
  z_vec[0] = h_2;
  z_vec[1] = k_2;
  z_vec[2] = l_2;
  
  this->_crystallographic_directions[0][0] = x_vec[0]; 
  this->_crystallographic_directions[0][1] = x_vec[1];
  this->_crystallographic_directions[0][2] = x_vec[2];
  
  this->_crystallographic_directions[2][0] = z_vec[0]; 
  this->_crystallographic_directions[2][1] = z_vec[1];
  this->_crystallographic_directions[2][2] = z_vec[2];
  
  crossProduct(x_vec, z_vec, y_vec);
  
  this->_crystallographic_directions[1][0] = y_vec[0]; 
  this->_crystallographic_directions[1][1] = y_vec[1];
  this->_crystallographic_directions[1][2] = y_vec[2];

  delete [] x_vec;
  delete [] y_vec;
  delete [] z_vec;
}
void RawStateOrient::Generator::_standardOrientation()
{
  this->_crystallographic_directions[0][0] = 1;
  this->_crystallographic_directions[0][1] = 0;
  this->_crystallographic_directions[0][2] = 0;

  this->_crystallographic_directions[1][0] = 0;
  this->_crystallographic_directions[1][1] = 1;
  this->_crystallographic_directions[1][2] = 0;

  this->_crystallographic_directions[2][0] = 0;
  this->_crystallographic_directions[2][1] = 0;
  this->_crystallographic_directions[2][2] = 1;
}
void RawStateOrient::Generator::_setLatticeConstants(double a, double b, double c)
{
  this->_lattice_constants[0] = a;
  this->_lattice_constants[1] = b;
  this->_lattice_constants[2] = c;
}
void RawStateOrient::Generator::_setLatticeAngles(double alpha, double beta, double gamma)
{
  this->_lattice_angles[0] = (alpha/180)*M_PI;
  this->_lattice_angles[1] = (beta/180)*M_PI;
  this->_lattice_angles[2] = (gamma/180)*M_PI;
}
void RawStateOrient::Generator::_generateBasicVectors()
{
  double lx, ly, lz;
  double xy, xz, yz;

  lx = this->_lattice_constants[0];
  xy = this->_lattice_constants[1] * cos(this->_lattice_angles[2]);
  xz = this->_lattice_constants[2] * cos(this->_lattice_angles[1]);
  ly = sqrt( ( this->_lattice_constants[1] * this->_lattice_constants[1] ) - ( xy * xy ) );
  yz = ( ( this->_lattice_constants[1] * this->_lattice_constants[2] * cos( this->_lattice_angles[0] ) ) - ( xy * xz ) ) / ly;
  lz = sqrt( ( this->_lattice_constants[2] * this->_lattice_constants[2] ) - ( xz * xz ) - ( yz * yz ) );

  this->_basic_vectors[0][0] = lx;
  this->_basic_vectors[0][1] = 0.0;
  this->_basic_vectors[0][2] = 0.0;

  this->_basic_vectors[1][0] = xy;
  this->_basic_vectors[1][1] = ly;
  this->_basic_vectors[1][2] = 0.0;

  this->_basic_vectors[2][0] = xz;
  this->_basic_vectors[2][1] = yz;
  this->_basic_vectors[2][2] = lz;
  
  for (int i = 0; i < 3; ++i) 
    for (int j = 0; j < 3; ++j)
      if(this->_basic_vectors[i][j] < EPSILON && this->_basic_vectors[i][j] > -EPSILON) this->_basic_vectors[i][j] = 0.0;
}
void RawStateOrient::Generator::_setPointOfOrigin(double x, double y, double z)
{
  this->_point_of_origin[0] = x;
  this->_point_of_origin[1] = y;
  this->_point_of_origin[2] = z;
}
void RawStateOrient::Generator::_zeroOrigin()
{
  for (int coordinate = 0; coordinate < 3; ++coordinate) this->_point_of_origin[coordinate] = 0.0;
}
void RawStateOrient::Generator::_generateAtomicBasis()
{
  if(this->_lattice_type.find("fcc") != std::string::npos) this->_basis_population = 4;   
  if(this->_lattice_type.find("bcc") != std::string::npos) this->_basis_population = 2;   
  if(this->_lattice_type.find("sc") != std::string::npos) this->_basis_population = 1;   
  if(this->_lattice_type.find("dia") != std::string::npos) this->_basis_population = 8;   
  if(this->_lattice_type.find("hex") != std::string::npos) this->_basis_population = 2;   

  this->_atomic_basis = new double *[this->_basis_population];
  for(int atom = 0; atom < this->_basis_population; atom++)
    this->_atomic_basis[atom] = new double [3];

  if(this->_lattice_type.find("fcc") != std::string::npos)
  {
    this->_atomic_basis[0][0] = 0;
    this->_atomic_basis[0][1] = 0;
    this->_atomic_basis[0][2] = 0;

    this->_atomic_basis[1][0] = 0;
    this->_atomic_basis[1][1] = 0.5;
    this->_atomic_basis[1][2] = 0.5;

    this->_atomic_basis[2][0] = 0.5;
    this->_atomic_basis[2][1] = 0.5;
    this->_atomic_basis[2][2] = 0;

    this->_atomic_basis[3][0] = 0.5;
    this->_atomic_basis[3][1] = 0;
    this->_atomic_basis[3][2] = 0.5;
  }
  if(this->_lattice_type.find("bcc") != std::string::npos)
  {
    this->_atomic_basis[0][0] = 0;
    this->_atomic_basis[0][1] = 0;
    this->_atomic_basis[0][2] = 0;

    this->_atomic_basis[1][0] = 0.5;
    this->_atomic_basis[1][1] = 0.5;
    this->_atomic_basis[1][2] = 0.5;
  }
  if(this->_lattice_type.find("sc") != std::string::npos)
  {
    this->_atomic_basis[0][0] = 0;
    this->_atomic_basis[0][1] = 0;
    this->_atomic_basis[0][2] = 0;
  }
  if(this->_lattice_type.find("dia") != std::string::npos)
  {
    this->_atomic_basis[0][0] = 0;
    this->_atomic_basis[0][1] = 0;
    this->_atomic_basis[0][2] = 0;

    this->_atomic_basis[1][0] = 0;
    this->_atomic_basis[1][1] = 0.5;
    this->_atomic_basis[1][2] = 0.5;

    this->_atomic_basis[2][0] = 0.5;
    this->_atomic_basis[2][1] = 0.5;
    this->_atomic_basis[2][2] = 0;

    this->_atomic_basis[3][0] = 0.5;
    this->_atomic_basis[3][1] = 0;
    this->_atomic_basis[3][2] = 0.5;

    this->_atomic_basis[4][0] = 0.75;
    this->_atomic_basis[4][1] = 0.25;
    this->_atomic_basis[4][2] = 0.25;

    this->_atomic_basis[5][0] = 0.75;
    this->_atomic_basis[5][1] = 0.75;
    this->_atomic_basis[5][2] = 0.75;

    this->_atomic_basis[6][0] = 0.25;
    this->_atomic_basis[6][1] = 0.75;
    this->_atomic_basis[6][2] = 0.25;

    this->_atomic_basis[7][0] = 0.25;
    this->_atomic_basis[7][1] = 0.25;
    this->_atomic_basis[7][2] = 0.75;
  }
  if(this->_lattice_type.find("hex") != std::string::npos)
  {
    this->_atomic_basis[0][0] = 0;
    this->_atomic_basis[0][1] = 0;
    this->_atomic_basis[0][2] = 0;

    this->_atomic_basis[1][0] = 0.33;
    this->_atomic_basis[1][1] = 0.66;
    this->_atomic_basis[1][2] = 0.5;
  }
}
void RawStateOrient::Generator::_setLatticeType(std::string lattice_type)
{
  this->_lattice_type = lattice_type;

  if ( lattice_type.find("hex") != std::string::npos ) this->_setLatticeAngles(90,90,120);
  if ( lattice_type.find("fcc") != std::string::npos ) this->_setLatticeAngles(90,90,90);
  if ( lattice_type.find("bcc") != std::string::npos ) this->_setLatticeAngles(90,90,90);
  if ( lattice_type.find("sc") != std::string::npos ) this->_setLatticeAngles(90,90,90);

  this->_generateAtomicBasis();
  this->_generateBasicVectors();
}
void RawStateOrient::Generator::_loadCustomLattice(std::string file_path)
{
  std::ifstream input_structure;
  
  input_structure.open(file_path.c_str());

  std::string dataline; 
  std::stringstream ss_dataline;

  int line_counter = 0; 

  while(getline(input_structure, dataline))
  {
    this->_atomic_basis[line_counter] = new double [3];

    ss_dataline.str(dataline);

    for (int coordinate = 0; coordinate < 3; ++coordinate)
      ss_dataline >> this->_atomic_basis[line_counter][coordinate];

    line_counter++;
    ss_dataline.str("");
    ss_dataline.clear();
  }
  input_structure.close();

  this->_basis_population = line_counter;
}
void RawStateOrient::Generator::_setNumberOfRepetitions(int repetitions_x, int repetitions_y, int repetitions_z)
{
  this->_number_of_repetitions[0] = repetitions_x;
  this->_number_of_repetitions[1] = repetitions_y;
  this->_number_of_repetitions[2] = repetitions_z;
}
RawStateOrient::Generator::Generator()
{
  this->_crystallographic_directions = new int *[3];
  for (int row = 0; row < 3; ++row)
    this->_crystallographic_directions[row] = new int [3]; 
  
  this->_lattice_constants = new double [3];
  this->_lattice_angles = new double [3];

  this->_basic_vectors = new double *[3];
  for (int row = 0; row < 3; ++row)
    this->_basic_vectors[row] = new double [3]; 

  this->_point_of_origin = new double [3];
  this->_number_of_repetitions = new int [3];

  this->_zeroOrigin();
  this->_standardOrientation();
}
RawStateOrient::Generator::~Generator()
{
  if(this->_crystallographic_directions != nullptr)
  {
    for (int row = 0; row < 3; ++row)
      delete [] this->_crystallographic_directions[row];
    delete [] this->_crystallographic_directions;
  }

  if(this->_lattice_constants != nullptr)
    delete [] this->_lattice_constants;
  
  if(this->_lattice_angles != nullptr)
    delete [] this->_lattice_angles;

  if(this->_basic_vectors != nullptr)
  {
    for (int row = 0; row < 3; ++row)
      delete [] this->_basic_vectors[row];
    delete [] this->_basic_vectors;
  }

  if(this->_point_of_origin != nullptr)
    delete [] this->_point_of_origin;
  
  if(this->_number_of_repetitions != nullptr)
    delete [] this->_number_of_repetitions;

  if(this->_atomic_basis != nullptr)
  {
    for (int atom = 0; atom < 3; ++atom)
      delete [] this->_atomic_basis[atom];
    delete [] this->_atomic_basis;
  }
}
void RawStateOrient::growSampleFromBasis(Generator *growth_info, RawState::System *crystal)
{
  double original_basis_norms[3];

  for(int vector = 0; vector < 3; vector++)
    original_basis_norms[vector] = sqrt( growth_info->_basic_vectors[vector][0] * growth_info->_basic_vectors[vector][0] +
                                  growth_info->_basic_vectors[vector][1] * growth_info->_basic_vectors[vector][1] +
                                  growth_info->_basic_vectors[vector][2] * growth_info->_basic_vectors[vector][2] );
  int minimum_miller[3];
  int maximum_miller[3];

  for(int i = 0; i < 3; i++)
  {
    minimum_miller[i] = INT_MAX;
    maximum_miller[i] = -INT_MAX;
  }

  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
    {
      if(growth_info->_crystallographic_directions[j][i] > maximum_miller[i]){maximum_miller[i] = growth_info->_crystallographic_directions[j][i];}
      if(growth_info->_crystallographic_directions[j][i] < minimum_miller[i]){minimum_miller[i] = growth_info->_crystallographic_directions[j][i];}
    }

  int number_of_cells = 0;

  for (int rep_a = minimum_miller[0] - 2; rep_a < maximum_miller[0] + 2; ++rep_a) 
    for (int rep_b = minimum_miller[1] - 2; rep_b < maximum_miller[1] + 2; ++rep_b) 
      for (int rep_c = minimum_miller[2] - 2; rep_c < maximum_miller[2] + 2; ++rep_c)
        number_of_cells++;
  
  int crystal_population = growth_info->_basis_population * number_of_cells;
  
  crystal->_setNAtoms(crystal_population);
  crystal->_createParticlesTableWithPositions();

  int atom_counter = 0;

  for (int basis_atom = 0; basis_atom < growth_info->_basis_population; ++basis_atom)
    for (int rep_a = minimum_miller[0] - 2; rep_a < maximum_miller[0] + 2; ++rep_a) 
      for (int rep_b = minimum_miller[1] - 2; rep_b < maximum_miller[1] + 2; ++rep_b) 
        for (int rep_c = minimum_miller[2] - 2; rep_c < maximum_miller[2] + 2; ++rep_c)
        {
          crystal->_particles[atom_counter]._setExistance(true);
          crystal->_particles[atom_counter]._setTypeId(1);
          crystal->_particles[atom_counter]._position[0] = growth_info->_atomic_basis[basis_atom][0] * original_basis_norms[0] 
                                                          + rep_a * growth_info->_basic_vectors[0][0] 
                                                          + rep_b * growth_info->_basic_vectors[1][0]
                                                          + rep_c * growth_info->_basic_vectors[2][0] + SHIFT;
          crystal->_particles[atom_counter]._position[1] = growth_info->_atomic_basis[basis_atom][1] * original_basis_norms[1]
                                                          + rep_a * growth_info->_basic_vectors[0][1]
                                                          + rep_b * growth_info->_basic_vectors[1][1]
                                                          + rep_c * growth_info->_basic_vectors[2][1] + SHIFT;
          crystal->_particles[atom_counter]._position[2] = growth_info->_atomic_basis[basis_atom][2] * original_basis_norms[2]
                                                          + rep_a * growth_info->_basic_vectors[0][2] 
                                                          + rep_b * growth_info->_basic_vectors[1][2] 
                                                          + rep_c * growth_info->_basic_vectors[2][2] + SHIFT;
          atom_counter++; 
        }
  
  double **target_matrix = new double *[3];
  for (int row = 0; row < 3; ++row)
    target_matrix[row] = new double [3];
  
  for (int row = 0; row < 3; ++row)
    for (int column = 0; column < 3; ++column)
      target_matrix[row][column] = growth_info->_crystallographic_directions[row][column];
  
  double **rotated_vectors = new double *[3];
  for (int vector = 0; vector < 3; vector++)
    rotated_vectors[vector] = new double [3];
  
  rotated_vectors[0][0] = target_matrix[0][0] * growth_info->_basic_vectors[0][0] +
                          target_matrix[0][1] * growth_info->_basic_vectors[1][0] +
                          target_matrix[0][2] * growth_info->_basic_vectors[2][0];

  rotated_vectors[0][1] = target_matrix[0][0] * growth_info->_basic_vectors[0][1] +
                          target_matrix[0][1] * growth_info->_basic_vectors[1][1] +
                          target_matrix[0][2] * growth_info->_basic_vectors[2][1];
  
  rotated_vectors[0][2] = target_matrix[0][0] * growth_info->_basic_vectors[0][2] +
                          target_matrix[0][1] * growth_info->_basic_vectors[1][2] +
                          target_matrix[0][2] * growth_info->_basic_vectors[2][2];
  
  rotated_vectors[1][0] = target_matrix[1][0] * growth_info->_basic_vectors[0][0] +
                          target_matrix[1][1] * growth_info->_basic_vectors[1][0] +
                          target_matrix[1][2] * growth_info->_basic_vectors[2][0];

  rotated_vectors[1][1] = target_matrix[1][0] * growth_info->_basic_vectors[0][1] +
                          target_matrix[1][1] * growth_info->_basic_vectors[1][1] +
                          target_matrix[1][2] * growth_info->_basic_vectors[2][1];
  
  rotated_vectors[1][2] = target_matrix[1][0] * growth_info->_basic_vectors[0][2] +
                          target_matrix[1][1] * growth_info->_basic_vectors[1][2] +
                          target_matrix[1][2] * growth_info->_basic_vectors[2][2];

  rotated_vectors[2][0] = target_matrix[2][0] * growth_info->_basic_vectors[0][0] +
                          target_matrix[2][1] * growth_info->_basic_vectors[1][0] +
                          target_matrix[2][2] * growth_info->_basic_vectors[2][0];

  rotated_vectors[2][1] = target_matrix[2][0] * growth_info->_basic_vectors[0][1] +
                          target_matrix[2][1] * growth_info->_basic_vectors[1][1] +
                          target_matrix[2][2] * growth_info->_basic_vectors[2][1];
  
  rotated_vectors[2][2] = target_matrix[2][0] * growth_info->_basic_vectors[0][2] +
                          target_matrix[2][1] * growth_info->_basic_vectors[1][2] +
                          target_matrix[2][2] * growth_info->_basic_vectors[2][2];
  
  double **original_vectors_normed = new double *[3];
  for (int vector = 0; vector < 3; vector++)
    original_vectors_normed[vector] = new double [3];
  
  double **rotated_vectors_normed = new double *[3];
  for (int vector = 0; vector < 3; vector++)
    rotated_vectors_normed[vector] = new double [3];

  double *rotated_basis_norms = new double [3];
  
  for(int vector = 0; vector < 3; vector++)
    rotated_basis_norms[vector] = sqrt( rotated_vectors[vector][0] * rotated_vectors[vector][0] +
                                        rotated_vectors[vector][1] * rotated_vectors[vector][1] +
                                        rotated_vectors[vector][2] * rotated_vectors[vector][2] );

  for (int vector = 0; vector < 3; vector++)
    for (int component = 0; component < 3; component++)
    {
      original_vectors_normed[vector][component] = growth_info->_basic_vectors[vector][component] / original_basis_norms[vector];
      rotated_vectors_normed[vector][component] = rotated_vectors[vector][component] / rotated_basis_norms[vector];
    }

  printMat3by3( original_vectors_normed );
  printMat3by3( rotated_vectors_normed );

  double **rotation_matrix = new double *[3];
  for (int row = 0; row < 3; ++row)
    rotation_matrix[row] = new double [3];
  
  double **inverted_rotation_matrix = new double *[3];
  for (int row = 0; row < 3; ++row)
    inverted_rotation_matrix[row] = new double [3];
  
  calculateRotationMatrix(original_vectors_normed, rotated_vectors_normed, rotation_matrix);
  int exit_code = matrixInversion(rotation_matrix, 3, inverted_rotation_matrix); 
  if(exit_code == 1)
  {
    cout << "CANNOT INVERT ROTATION MATRIX!\n";
    exit(1);
  }

  printMat3by3(rotation_matrix);

  rotateAtomPositions( crystal, inverted_rotation_matrix ) ;
  rotateBasisVectors( rotated_vectors, inverted_rotation_matrix, rotated_vectors );
  
  //generalParallelepipedToTriclinic( rotated_vectors );
  
  for(int vector = 0; vector < 3; vector++)
    rotated_basis_norms[vector] = sqrt( rotated_vectors[vector][0] * rotated_vectors[vector][0] +
                                        rotated_vectors[vector][1] * rotated_vectors[vector][1] +
                                        rotated_vectors[vector][2] * rotated_vectors[vector][2] );
  
  for (int atom = 0; atom < crystal->_n_atoms; atom++)
  {
    double x_comp = 0.0;
    double y_comp = 0.0;
    double z_comp = 0.0;

    for (int column = 0; column < 3; ++column)
    {
      x_comp += ( rotated_vectors[0][column] / rotated_basis_norms[0] ) * crystal->_particles[atom]._position[column];
      y_comp += ( rotated_vectors[1][column] / rotated_basis_norms[1] ) * crystal->_particles[atom]._position[column];
      z_comp += ( rotated_vectors[2][column] / rotated_basis_norms[2] ) * crystal->_particles[atom]._position[column];
    }

    x_comp /= rotated_basis_norms[0];
    y_comp /= rotated_basis_norms[1];
    z_comp /= rotated_basis_norms[2];

    bool x_in = false;
    bool y_in = false;
    bool z_in = false;

    if( x_comp < 1.0 - EPSILON && x_comp > -EPSILON ) x_in = true; 
    if( y_comp < 1.0 - EPSILON && y_comp > -EPSILON ) y_in = true; 
    if( z_comp < 1.0 - EPSILON && z_comp > -EPSILON ) z_in = true; 

    if( x_in && y_in && z_in ) crystal->_particles[atom]._setExistance( true );
    else crystal->_particles[atom]._setExistance( false );
  }

  double xlo = growth_info->_point_of_origin[0];
  double xhi = growth_info->_point_of_origin[0] + rotated_vectors[0][0];
  
  double ylo = growth_info->_point_of_origin[1];
  double yhi = growth_info->_point_of_origin[1] + rotated_vectors[1][1];
  
  double zlo = growth_info->_point_of_origin[2];
  double zhi = growth_info->_point_of_origin[2] + rotated_vectors[2][2];

  double xy = rotated_vectors[1][0]; 
  double xz = rotated_vectors[2][0];
  double yz = rotated_vectors[2][1];

  double xlo_bound = xlo + std::min(std::min(std::min(0.0, xy), xz), xy + xz);
  double xhi_bound = xhi + std::max(std::min(std::min(0.0, xy), xz), xy + xz);
  double ylo_bound = ylo + std::min(0.0, yz);
  double yhi_bound = yhi + std::max(0.0, yz);
  double zlo_bound = zlo;
  double zhi_bound = zhi;
  
  crystal->_box_data[0][0] = xlo_bound;
  crystal->_box_data[0][1] = xhi_bound;
  crystal->_box_data[0][2] = xy;
  
  crystal->_box_data[1][0] = ylo_bound;
  crystal->_box_data[1][1] = yhi_bound;
  crystal->_box_data[1][2] = xz;
  
  crystal->_box_data[2][0] = zlo_bound;
  crystal->_box_data[2][1] = zhi_bound;
  crystal->_box_data[2][2] = yz;
  
  for (int row = 0; row < 3; ++row)
    delete [] target_matrix[row];
  delete [] target_matrix;
  
  for (int row = 0; row < 3; ++row)
    delete [] rotation_matrix[row];
  delete [] rotation_matrix;
  
  for (int row = 0; row < 3; ++row)
    delete [] inverted_rotation_matrix[row];
  delete [] inverted_rotation_matrix;
  
  for (int vector = 0; vector < 3; vector++)
  {
    delete [] original_vectors_normed[vector];
    delete [] rotated_vectors[vector];
    delete [] rotated_vectors_normed[vector];
  }

  delete [] original_vectors_normed;
  delete [] rotated_vectors;
  delete [] rotated_vectors_normed;
}
  
/*
  
  for (int atom = 0; atom < crystal->_n_atoms; atom++)
  {
      
    crystal->_particles[atom]._type_id = 1;
  }

  for (int vector = 0; vector < 3; vector++)
  {
    for (int component = 0; component < 3; component++)
      cout << rotated_vectors[vector][component] << " ";
    cout << "\n";
  }
  cout << "\n";
  */

  /*

  int rep_counter[3] = {0,0,0};
    
  for (int rep_a = 0; rep_a < growth_info->_number_of_repetitions[0]; ++rep_a) 
    rep_counter[0]++;
  for (int rep_b = 0; rep_b < growth_info->_number_of_repetitions[1]; ++rep_b) 
    rep_counter[1]++;
  for (int rep_c = 0; rep_c < growth_info->_number_of_repetitions[2]; ++rep_c)
    rep_counter[2]++;

  double xlo = growth_info->_point_of_origin[0];
  double xhi = growth_info->_point_of_origin[0] + rotated_vectors[0][0];
  
  double ylo = growth_info->_point_of_origin[1];
  double yhi = growth_info->_point_of_origin[1] + rotated_vectors[1][1];
  
  double zlo = growth_info->_point_of_origin[2];
  double zhi = growth_info->_point_of_origin[2] + rotated_vectors[2][2];

  double xy = growth_info->_basic_vectors[1][0]; 
  double xz = growth_info->_basic_vectors[2][0];
  double yz = growth_info->_basic_vectors[2][1];
 
  double xlo_bound = xlo + std::min(std::min(std::min(0.0, xy), xz), xy + xz);
  double xhi_bound = xhi + std::max(std::min(std::min(0.0, xy), xz), xy + xz);
  double ylo_bound = ylo + std::min(0.0, yz);
  double yhi_bound = yhi + std::max(0.0, yz);
  double zlo_bound = zlo;
  double zhi_bound = zhi;

  crystal->_box_data[0][0] = xlo_bound;
  crystal->_box_data[0][1] = xhi_bound;
  crystal->_box_data[0][2] = xy;
  
  crystal->_box_data[1][0] = ylo_bound;
  crystal->_box_data[1][1] = yhi_bound;
  crystal->_box_data[1][2] = xz;
  
  crystal->_box_data[2][0] = zlo_bound;
  crystal->_box_data[2][1] = zhi_bound;
  crystal->_box_data[2][2] = yz;

  */

  // KATY

  /*
  // ALPHA -> B dot C
  // BETA -> A dot C
  // GAMMA -> A dot B

  double alpha_cosine, beta_cosine, gamma_cosine;
    
  double alpha_product = 0;
  double beta_product = 0;
  double gamma_product = 0;

  for (int column = 0; column < 3; ++column)
  {
    alpha_product += rotated_vectors[1][column] * rotated_vectors[2][column]; 
    beta_product += rotated_vectors[0][column] * rotated_vectors[2][column]; 
    gamma_product += rotated_vectors[0][column] * rotated_vectors[1][column]; 
  }

  alpha_cosine = alpha_product / ( rotated_basis_norms[1] * rotated_basis_norms[2] );
  beta_cosine = beta_product / ( rotated_basis_norms[0] * rotated_basis_norms[2] );
  gamma_cosine = gamma_product / ( rotated_basis_norms[0] * rotated_basis_norms[1] );

  double lx = rotated_basis_norms[0];
  double xy = rotated_basis_norms[1] * gamma_cosine;
  double xz = rotated_basis_norms[2] * beta_cosine;
  double ly = sqrt( rotated_basis_norms[1] * rotated_basis_norms[1] - xy * xy ); 
  double yz = ( rotated_basis_norms[1] * rotated_basis_norms[2] - xy * xz ) / ly;
  double lz = sqrt( rotated_basis_norms[2] * rotated_basis_norms[2] - xz * xz - yz * yz );
  
  double xlo_bound = growth_info->_point_of_origin[0] + std::min(std::min(std::min(0.0, xy), xz), xy + xz);
  double xhi_bound = lx + std::max(std::min(std::min(0.0, xy), xz), xy + xz);
  double ylo_bound = growth_info->_point_of_origin[1] + std::min(0.0, yz);
  double yhi_bound = ly + std::max(0.0, yz);
  double zlo_bound = growth_info->_point_of_origin[2];
  double zhi_bound = lz;
  
  crystal->_box_data[0][0] = xlo_bound;
  crystal->_box_data[0][1] = xhi_bound;
  crystal->_box_data[0][2] = xy;
  
  crystal->_box_data[1][0] = ylo_bound;
  crystal->_box_data[1][1] = yhi_bound;
  crystal->_box_data[1][2] = xz;
  
  crystal->_box_data[2][0] = zlo_bound;
  crystal->_box_data[2][1] = zhi_bound;
  crystal->_box_data[2][2] = yz;

  */
