/**
 * @file
 * @brief This file contains misc. mathematical tools which may come in handy during
 * postprocessing of simulation data ( matrix inversion, curve fitting via GNUPlot handler etc. )
 * 
 * @author MSc. Eng. Kamil Rybacki
 * 
 * This module contain the following mathematical tools:@n
 * 
 * 1) Matrix inversion for any kind of data-type (double, float etc)@n
 *
 * @date 28/04/2020
 * 
 */

#ifndef RAWSTATE_MATH_TOOLS
#define RAWSTATE_MATH_TOOLS

namespace RawStateMathTools
{
    /**
     *
     * @brief Calculates minor located within the input matrix 
     * 
     */
  void computeRotationMatrix(double **initial_basis, double **target_basis, double **output_matrix);
    /**
     *
     * @brief Calculates minor located within the input matrix 
     * 
     */
  int getMinor(double **src, double **dest, int row, int col, int order);
    /**
     *
     * @brief  
     * 
     */
  double calcDeterminant(double **mat, int order);
    /**
     *
     * @brief 
     * 
     */
  int matrixInversion(double **A, int order, double **Y);
    /**
     *
     * @brief 
     * 
     */
  void printTensor(double **A, int order);
}

#endif /* ifndef RAWSTATE_MATH_TOOLS */
