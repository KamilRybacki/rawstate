#ifndef RAWSTATE_ORIENT_H
#define RAWSTATE_ORIENT_H

#include "../rawstate.h"

namespace RawStateOrient
{
  struct Generator
  {
    int **_crystallographic_directions;
    
    void _setCrystallographicDirections(int h_1, int k_1, int l_1, int h_2, int k_2, int l_2);
    void _standardOrientation(); 

    double *_lattice_constants;
    void _setLatticeConstants(double a, double b, double c);

    double *_lattice_angles;
    void _setLatticeAngles(double alpha, double beta, double gamma);
    
    double **_basic_vectors;

    void _generateBasicVectors();

    double *_point_of_origin;
    
    void _setPointOfOrigin(double x, double y, double z);
    void _zeroOrigin();

    int _basis_population;
    double **_atomic_basis;
    
    void _generateAtomicBasis();

    std::string _lattice_type;

    void _setLatticeType(std::string lattice_type);
    void _loadCustomLattice(std::string file_path);

    int *_number_of_repetitions;

    void _setNumberOfRepetitions(int repetitions_x, int repetitions_y, int repetitions_z);
    
    Generator();
    ~Generator();
  }; 

  void growSampleFromBasis(Generator *growth_info, RawState::System *crystal);
}

#endif /* RAWSTATE_ORIENT_H */
