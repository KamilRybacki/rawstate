#include <iostream>
#include <cmath>
#include <vector>

#include "rawstate_msd.h"
#include "../rawstate.h"

using std::cout;

RawStateMSD::MSD::MSD()
{
  this->_initial_state_atoms = nullptr;
  this->_previous_state_atoms = nullptr;
  this->_actual_state_atoms = nullptr;
  this->_msd = new double [4];
  this->_actual_box_lengths = new double [3];
  this->_is_periodic = new bool [3];
}
RawStateMSD::MSD::~MSD()
{
  if(this->_initial_state_atoms != nullptr) delete [] this->_initial_state_atoms;
  if(this->_previous_state_atoms != nullptr) delete [] this->_previous_state_atoms;
  if(this->_actual_box_lengths != nullptr) delete [] this->_actual_box_lengths;
  if(this->_actual_state_atoms != nullptr) delete [] this->_actual_state_atoms;
  if(this->_is_periodic != nullptr) delete [] this->_is_periodic;
  if(this->_msd != nullptr) delete [] this->_msd;
}
void RawStateMSD::MSD::_setNumberOfAtoms(int n_atoms)
{
  this->_n_atoms = n_atoms;
}
void RawStateMSD::MSD::_createAtomTables()
{
  if(this->_initial_state_atoms == nullptr) this->_initial_state_atoms = new RawState::Atom [this->_n_atoms];
  if(this->_previous_state_atoms == nullptr) this->_previous_state_atoms = new RawState::Atom [this->_n_atoms];
  if(this->_actual_state_atoms == nullptr) this->_actual_state_atoms = new RawState::Atom [this->_n_atoms];
  for (int atom_i = 0; atom_i < this->_n_atoms; ++atom_i)
  {
    this->_initial_state_atoms[atom_i]._createPositionsTable();
    this->_previous_state_atoms[atom_i]._createPositionsTable();
    this->_actual_state_atoms[atom_i]._createPositionsTable();
  }
}
void RawStateMSD::MSD::_storeInitialState(RawState::System *&input, bool if_norm)
{
  input->_calculateBasisVectors();
	input->_wrapAtoms(if_norm);
  for (int atom_i = 0; atom_i < this->_n_atoms; ++atom_i)
  {
    for (int dimension = 0; dimension < 3; ++dimension)
    {
      this->_initial_state_atoms[atom_i]._position[dimension] = input->_particles[atom_i]._position[dimension + 3];
      this->_actual_state_atoms[atom_i]._position[dimension] = input->_particles[atom_i]._position[dimension + 3];
      this->_previous_state_atoms[atom_i]._position[dimension] = input->_particles[atom_i]._position[dimension + 3];
    }
  }
  for (int dimension = 0; dimension < 3; ++dimension)
  {
    this->_is_periodic[dimension] = input->_periodic[dimension];
    this->_actual_box_lengths[dimension] = input->_basis_vectors[dimension][3];
  }
}
void RawStateMSD::MSD::_storeActualState(RawState::System *&input, bool if_norm)
{
  for (int atom_i = 0; atom_i < this->_n_atoms; ++atom_i)
  {
    for (int dimension = 0; dimension < 3; ++dimension)
      this->_previous_state_atoms[atom_i]._position[dimension] = this->_actual_state_atoms[atom_i]._position[dimension];
  }
  input->_calculateBasisVectors();
	input->_wrapAtoms(if_norm);
  for (int i = 0; i < 3; ++i)
    this->_actual_box_lengths[i] = input->_basis_vectors[i][3];
  for (int atom_i = 0; atom_i < this->_n_atoms; ++atom_i)
  {
    for (int dimension = 0; dimension < 3; ++dimension)
      this->_actual_state_atoms[atom_i]._position[dimension] = input->_particles[atom_i]._position[dimension + 3];
  }
}
void RawStateMSD::MSD::_calculateMSD()
{
	double *displacement = new double [3];

  for (int dimension = 0; dimension < 4; ++dimension)
    this->_msd[dimension] = 0.0;

  for (int atom_i = 0; atom_i < this->_n_atoms; ++atom_i)
  {
    for (int dimension = 0; dimension < 3; ++dimension)
    {
      displacement[dimension] = (this->_actual_state_atoms[atom_i]._position[dimension] - this->_previous_state_atoms[atom_i]._position[dimension]);

      if(this->_is_periodic[dimension] == true)
      {
        if(displacement[dimension] > 0.5) this->_initial_state_atoms[atom_i]._position[dimension] += 1.0;   
        if(displacement[dimension] <= -0.5) this->_initial_state_atoms[atom_i]._position[dimension] -= 1.0;   
      } 
      displacement[dimension] = (this->_actual_state_atoms[atom_i]._position[dimension] - this->_initial_state_atoms[atom_i]._position[dimension]);
      this->_msd[dimension] += displacement[dimension] * displacement[dimension];
    }
  }
  for (int dimension = 0; dimension < 3; ++dimension)
  {
    this->_msd[dimension] = this->_msd[dimension] / this->_n_atoms;
    this->_msd[3] += this->_msd[dimension] * this->_msd[dimension];
  }
  this->_msd[3] = sqrt(this->_msd[3]);

  delete [] displacement;
}
void RawStateMSD::calculateMSDForLAMMPSDump(std::string filename, std::string outpath, int start, int n_every, int n_windows)
{
  RawState::setStateTimestep(start);
  RawState::setStateTimestepDelta(n_every);
  RawState::System *test = new RawState::System;

  test->_readNextLAMMPSTimestep(filename);
  int first_label = test->_timestep_label;
  test->_readNextLAMMPSTimestep(filename);
  int second_label = test->_timestep_label;
  
  int dt = second_label - first_label;

  delete test;

  RawState::setStateTimestep(start);
  RawState::setStateTimestepDelta(n_every);
  int n_readings = RawStateLAMMPSTools::getNumberOfReadings(filename);

  RawState::System *system = new RawState::System;
	
  std::ofstream msd_output;
  std::string output_name;

  int n_readings_in_window = n_readings / n_windows;

  double **average_msd = new double *[n_readings_in_window];
  for (int reading = 0; reading < n_readings_in_window; ++reading)
  {
    average_msd[reading] = new double [4];
    for (int i = 0; i < 4; ++i)
      average_msd[reading][i] = 0.0;
  }

  for (int window = 0; window < n_windows; ++window)
  {
    cout << "Starting window No. " << window+1 << "\n";
    output_name = outpath;
    output_name += "/msd_";
    output_name += std::to_string(window + 1);
    output_name += ".out"; 
    msd_output.open(output_name.c_str());

    MSD *msd = new MSD;
  
    for(int reading = 0; reading < n_readings_in_window; reading++)
    {
      bool normalize = false;
      system->_readNextLAMMPSTimestep(filename);
      normalize = system->_normalize;
      if(reading == 0)
      {
        msd->_setNumberOfAtoms(system->_n_atoms);
        msd->_createAtomTables();
        msd->_storeInitialState(system, normalize);
      }
      if(reading > 0) msd->_storeActualState(system, normalize);
      
      msd->_calculateMSD();

      msd_output << reading * dt << " ";
      for (int i = 0; i < 4; ++i)
      {
        msd_output << msd->_msd[i] << " ";
        average_msd[reading][i] += msd->_msd[i];
      }
      msd_output << "\n";
    }

    cout << "Finished window No. " << window+1 << "!\n";
    delete msd;
    msd_output.close();
    msd_output.clear();
  }

  cout << "Calculating average MSD...\n";
  output_name = outpath;
  output_name += "/msd_average.out";

  msd_output.open(output_name.c_str());
  for (int reading = 0; reading < n_readings_in_window; ++reading)
  {
    msd_output << reading * dt << " ";
    for (int i = 0; i < 4; ++i)
      msd_output << average_msd[reading][i]/n_windows << " ";
    msd_output << "\n";
  }
  msd_output.close();

  cout << "Finished!\n";
  for (int reading = 0; reading < n_readings_in_window; ++reading)
    delete [] average_msd[reading];
  delete [] average_msd;

	delete system;
}
