#include <iostream>
#include <cmath>
#include <set>
#include <cfloat>
#include <cstdlib>
#include <climits>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>

#include "rawstate.h"

// This is the amount of shift used when creating atomic structures to ensure that they will be
// replicable in 3D space when Periodic Boundary Conditions are applied

#define SLIGHT_SHIFT 0.01

using namespace std;

int _state_timestep = 0;
int _state_timestep_delta = 1;

// Helper Functions

// Breakpoint functions for debugging

#define BP bp();
#define SBP(str) sbp((str));

int global_bp = 1;

void bp()
{
  cout << "BP" << global_bp << "\n";
  global_bp++;
}
void sbp(std::string text)
{
  cout << "BP: " << text << "\n";
}

void replaceSubstring(std::string& input, const std::string& key, const std::string& substitute) 
{
    size_t pos = 0;
    while ((pos = input.find(key, pos)) != std::string::npos)
    {
         input.replace(pos, key.length(), substitute);
         pos += key.length();
    }
}
int factorial(int n)
{
    if(n > 1)
        return n * factorial(n - 1);
    else
        return 1;
}
int inverse_factorial(int factorial){
    int current = 1;
    while (factorial > current) {
        if (factorial % current) {
            return -1; //not divisible
        }
        factorial /= current;
        ++current;
    }
    if (current == factorial) {
        return current;
    }
    return -1;
}

// RawState NS

void RawState::setStateTimestep(int state_timestep)
{
	_state_timestep = state_timestep;
}
int RawState::getStateTimestep()
{
	return _state_timestep;
}
void RawState::setStateTimestepDelta(int state_timestep_delta)
{
	_state_timestep_delta = state_timestep_delta;
}
int RawState::getStateTimestepDelta()
{
	return _state_timestep_delta;
}
bool RawState::checkBoundaryConditions(double *i_position, double *j_position, double norm_a, double norm_b, double norm_c, double r_cutoff, bool *is_periodic, double &bond_length)
{
	double da = 0.0;
	double db = 0.0;
	double dc = 0.0;

	double dr = 0.0;

  da = (j_position[3] - i_position[3]);
  db = (j_position[4] - i_position[4]);
  dc = (j_position[5] - i_position[5]);

	if(is_periodic[0] == true)
	{
		if(da > 0.5) da -= 1.0;   
		if(da <= -0.5) da += 1.0;   
	} 
	if(is_periodic[1] == true)
	{
		if(db > 0.5) db -= 1.0;   
		if(db <= -0.5) db += 1.0;   
	}
	if(is_periodic[2] == true)
	{
		if(dc > 0.5) dc -= 1.0;   
		if(dc <= -0.5) dc += 1.0;   
	}
  
	da *= norm_a;
	db *= norm_b;
	dc *= norm_c;

	dr = (da * da) + (db * db)  + (dc * dc);
	dr = sqrt(dr);
	bond_length = dr;

	bool final_check;

	if(dr <= r_cutoff) final_check = true;
	if(dr > r_cutoff) final_check = false;

	return final_check;
}

/*
struct ParseInfo
{
  int _n_atoms; 
  int _n_types; 
  std::string _type_labels; 
  std::string _description; 

  ParseInfo();
  ~ParseInfo();
};
*/

RawState::ParseInfo::ParseInfo()
{
  this->_n_atoms = 0;
  this->_n_types = 0;
  this->_type_labels = "";
  this->_description = "";
  this->_stechiometry_desc = "";
  this->_filepath = "";
}
void RawState::ParseInfo::_initializeStechiometry()
{
  this->_stechiometry = new int [this->_n_types];
}
RawState::ParseInfo::~ParseInfo()
{
  this->_type_labels = "";
  this->_description = "";
	if(this->_stechiometry != nullptr) delete [] this->_stechiometry;
}

/*
struct Atom
{
	bool _is_present;
	int _type_id;
	double *_position;
	double *_velocity;
	double *_force;
	double _mass;
  int _coordination_number;
};
*/

RawState::Atom::Atom()
{
	this->_is_present = true;
	this->_position = nullptr;
	this->_velocity = nullptr;
	this->_force = nullptr;
}
RawState::Atom::~Atom()
{
	delete [] this->_position;
	if(this->_velocity != nullptr) delete [] this->_velocity;
	if(this->_force != nullptr) delete [] this->_force;
}
void RawState::Atom::_setExistance(bool is_present)
{
	this->_is_present = is_present;
}
void RawState::Atom::_setMass(double mass)
{
	this->_mass = mass;
}
void RawState::Atom::_setTypeId(int type_id)
{
	this->_type_id = type_id;
}
void RawState::Atom::_createPositionsTable()
{
	/* First 3 entries are absolute coordinates; next 3 are partial coordinates */
	if(this->_position != nullptr) delete [] this->_position;
  this->_position = new double [6]; 
}
void RawState::Atom::_createVelocitiesTable()
{
	this->_velocity = new double [3];
}
void RawState::Atom::_createForcesTable()
{
	this->_force = new double [3];
}

/*
struct NeighbourList
{
	int _max_neighbours;
	double _r_cutoff;

	// Verlet list tables
	
	int _n_atoms;
	int *_n_neighbours;
	int **_neighbours_ids;
	double **_bond_lengths;
};
*/

RawState::NeighbourList::NeighbourList()
{
	this->_n_neighbours   = nullptr;
	this->_neighbours_ids = nullptr;
	this->_bond_lengths   = nullptr;
    
}
RawState::NeighbourList::~NeighbourList()
{
	for(int i = 0; i < this->_n_atoms; i++)
	{
		delete [] this->_neighbours_ids[i];	
		delete [] this->_bond_lengths[i];
	} 
	
	delete [] this->_neighbours_ids;	
	delete [] this->_bond_lengths;
  delete [] this->_n_neighbours;
}
void RawState::NeighbourList::_setNumberOfAtoms(int n_atoms)
{
	this->_n_atoms = n_atoms;
}
void RawState::NeighbourList::_setMaxNeighbours(int max_neighbours)
{
	this->_max_neighbours = max_neighbours;
}
void RawState::NeighbourList::_setCutoff(double r_cutoff)
{
	this->_r_cutoff = r_cutoff;
}
void RawState::NeighbourList::_prepareNeighboursList()
{
  if(this->_n_neighbours != nullptr) delete [] this->_n_neighbours;
  if(this->_neighbours_ids != nullptr)
  {
    for(int i = 0; i < this->_n_atoms; i++)
    {
      delete [] this->_neighbours_ids[i];	
    } 
    delete [] this->_neighbours_ids;
  }
  if(this->_bond_lengths != nullptr)
  {
    for(int i = 0; i < this->_n_atoms; i++)
    {
      delete [] this->_bond_lengths[i];	
    } 
    delete [] this->_bond_lengths;
  }

	this->_n_neighbours = new int [this->_n_atoms];
	this->_neighbours_ids = new int *[this->_n_atoms];
	this->_bond_lengths = new double *[this->_n_atoms];
	
	for(int i = 0; i < this->_n_atoms; i++)
	{
		this->_n_neighbours[i] = 0;
		this->_neighbours_ids[i] = new int [this->_max_neighbours];	
		this->_bond_lengths[i] = new double [this->_max_neighbours];	
	} 
}
void RawState::NeighbourList::_buildNeighboursList(double **box_vectors, Atom *atoms, bool *is_periodic)
{
	int atom_i, atom_j;

	for(atom_i = 0; atom_i < this->_n_atoms; atom_i++)
	{
		for(atom_j = atom_i + 1; atom_j < this->_n_atoms; atom_j++)
		{
			double r_ij;
      bool is_paired = false;
			is_paired = checkBoundaryConditions(atoms[atom_i]._position, atoms[atom_j]._position, box_vectors[0][3], box_vectors[1][3], box_vectors[2][3], this->_r_cutoff, is_periodic, r_ij);

			if(is_paired)
			{
				if(this->_n_neighbours[atom_j] == this->_max_neighbours ||
				   this->_n_neighbours[atom_i] == this->_max_neighbours)
				{
					std::cout << "Too many neighbours!\n";
					exit(0);
				}
						
				this->_neighbours_ids[atom_i][this->_n_neighbours[atom_i]] = atom_j;
				this->_neighbours_ids[atom_j][this->_n_neighbours[atom_j]] = atom_i;
					
				this->_bond_lengths[atom_i][this->_n_neighbours[atom_i]] = r_ij;
				this->_bond_lengths[atom_j][this->_n_neighbours[atom_j]] = r_ij;
						
				this->_n_neighbours[atom_i]++;
				this->_n_neighbours[atom_j]++;
			}
		}
	} 
}

/*
struct System
{	
	// Information about atoms 

	int _n_atoms;
	Atom *_particles;
	NeighbourList *_neighbours;
	double _r_cutoff;

	bool _toggle_velocites;
	bool _toggle_force;

	// Information about box shape and size
	
	double **_box_data;
	double **_basis_vectors;
	double *_point_of_origin;
	bool *_periodic;
};
*/
RawState::System::System()
{
	this->_timestep_label = 0;

	this->_particles        = nullptr;
	this->_neighbours       = nullptr;
  this->_parse_data       = nullptr;
  this->_misc_description = "none";

	this->_box_data = new double *[3];
	for(int i = 0; i < 3; i++)
		this->_box_data[i] = new double [3];
	
	this->_basis_vectors = new double *[3];
	for(int i = 0; i < 3; i++)
		this->_basis_vectors[i] = new double [4];

	this->_point_of_origin = new double [3];
	this->_periodic = new bool [3];
	
	this->_toggle_velocity = false;
	this->_toggle_force = false;
}
RawState::System::~System()
{
	if(this->_neighbours != nullptr) delete this->_neighbours;
	delete [] this->_periodic;
	if(this->_particles != nullptr) delete [] this->_particles;
	
	for(int i = 0; i < 3; i++)
		delete [] this->_box_data[i];
	delete [] this->_box_data;
	
	for(int i = 0; i < 3; i++)
		delete [] this->_basis_vectors[i];
	delete [] this->_basis_vectors;

	delete [] this->_point_of_origin;
	
  if(this->_parse_data != nullptr) delete this->_parse_data;
}
void RawState::System::_setBoxVertices(double **box_data)
{
	for(int i = 0; i < 3; i++)
		for(int j = 0; j < 3; j++)
			this->_box_data[i][j] = box_data[i][j];
}
void RawState::System::_calculateBasisVectors()
{
	double **triclinic_parameters = new double *[3];
	for(int dimension = 0; dimension < 3; dimension++)
		triclinic_parameters[dimension] = new double [2];

	vector<double> values;
	
  for(int i = 0; i < 3; i++)
		for (int j = 0; j < 4; j++)
      this->_basis_vectors[i][j] = 0.0;

	values.push_back(0.0);
	values.push_back(this->_box_data[0][2]);
	values.push_back(this->_box_data[1][2]);
	values.push_back(this->_box_data[0][2]+this->_box_data[1][2]);

	triclinic_parameters[0][0] = this->_box_data[0][0] - *min_element(values.begin(),values.end());
	triclinic_parameters[0][1] = this->_box_data[0][1] - *max_element(values.begin(),values.end());
	
	triclinic_parameters[1][0] = this->_box_data[1][0] - min(0.0, this->_box_data[2][2]);
	triclinic_parameters[1][1] = this->_box_data[1][1] - max(0.0, this->_box_data[2][2]);
	
	triclinic_parameters[2][0] = this->_box_data[2][0];
	triclinic_parameters[2][1] = this->_box_data[2][1];

	this->_point_of_origin[0] = triclinic_parameters[0][0];
	this->_point_of_origin[1] = triclinic_parameters[1][0];
	this->_point_of_origin[2] = triclinic_parameters[2][0];

	this->_basis_vectors[0][0] = triclinic_parameters[0][1] - triclinic_parameters[0][0];
	this->_basis_vectors[0][1] = 0.0;
	this->_basis_vectors[0][2] = this->_basis_vectors[0][1];

	this->_basis_vectors[1][0] = this->_box_data[0][2];
	this->_basis_vectors[1][1] = triclinic_parameters[1][1] - triclinic_parameters[1][0];
	this->_basis_vectors[1][2] = 0.0;

	this->_basis_vectors[2][0] = this->_box_data[1][2];
	this->_basis_vectors[2][1] = this->_box_data[2][2];
	this->_basis_vectors[2][2] = triclinic_parameters[2][1] - triclinic_parameters[2][0];

	for(int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			this->_basis_vectors[i][3] += this->_basis_vectors[i][j] *  this->_basis_vectors[i][j];
		this->_basis_vectors[i][3] = sqrt(this->_basis_vectors[i][3]);
	}
}
void RawState::System::_setCutoff(double r_cutoff)
{
	this->_r_cutoff = r_cutoff;
}
void RawState::System::_setNAtoms(int n_atoms)
{
	this->_n_atoms = n_atoms;
}
void RawState::System::_setPeriodicity(bool pa, bool pb, bool pc)
{
	this->_periodic[0] = pa;
	this->_periodic[1] = pb;
	this->_periodic[2] = pc;
}
void RawState::System::_createParticlesTable()
{
  if(this->_particles != nullptr) delete [] this->_particles;
	this->_particles = new Atom [this->_n_atoms];
}
void RawState::System::_createParticlesTableWithPositions()
{
  if(this->_particles != nullptr) delete [] this->_particles;
	this->_particles = new Atom [this->_n_atoms];
  for (int atom = 0; atom < this->_n_atoms; ++atom)
    this->_particles[atom]._createPositionsTable();
}
void RawState::System::_createParseData()
{
  if(this->_parse_data != nullptr) delete this->_parse_data;
  this->_parse_data = new ParseInfo;
}
void RawState::System::_normalizeAtoms()
{
  for(int atom_i = 0; atom_i < this->_n_atoms; atom_i++)
    for(int dimension = 0; dimension < 3; dimension++)
      this->_particles[atom_i]._position[3 + dimension] /= this->_basis_vectors[dimension][3] * this->_basis_vectors[dimension][3];
}
void RawState::System::_wrapAtoms(bool normalize)
{
  for(int atom_i = 0; atom_i < this->_n_atoms; atom_i++)
  {
    for(int dimension = 0; dimension < 3; dimension++)
    {
      this->_particles[atom_i]._position[3 + dimension] = 0.0;

      if(normalize)
      {
        this->_particles[atom_i]._position[dimension] -= this->_point_of_origin[dimension];
        
        for(int i = 0; i < 3; i++)
          this->_particles[atom_i]._position[3 + dimension] += this->_particles[atom_i]._position[i] * this->_basis_vectors[dimension][i]; 

        if( this->_particles[atom_i]._position[dimension + 3] < this->_basis_vectors[dimension][3] )
          this->_particles[atom_i]._position[dimension + 3] += this->_basis_vectors[dimension][3];
        if( this->_particles[atom_i]._position[dimension + 3] > this->_basis_vectors[dimension][3] )
          this->_particles[atom_i]._position[dimension + 3] -= this->_basis_vectors[dimension][3];
      }
      if(!normalize)
      {
        for(int i = 0; i < 3; i++)
          this->_particles[atom_i]._position[3 + dimension] += this->_particles[atom_i]._position[i]; 
      
        if( this->_particles[atom_i]._position[dimension + 3] < 0.0 )
          this->_particles[atom_i]._position[dimension + 3] += 1.0;
        if( this->_particles[atom_i]._position[dimension + 3] > 1.0 )
          this->_particles[atom_i]._position[dimension + 3] -= 1.0;
      }
    }
  }
  if(normalize) this->_normalizeAtoms();
}
void RawState::System::_enablePeriodicity()
{
  int n_atoms = this->_n_atoms;
  double epsilon = 0.005;

  double xp, yp, zp;

  for (int atom = 0; atom < n_atoms; ++atom)
  {
      xp = this->_particles[atom]._position[3];
      yp = this->_particles[atom]._position[4];
      zp = this->_particles[atom]._position[5];

      if(xp >= 1.0-epsilon && xp <= 1.0+epsilon) this->_particles[atom]._setExistance(false);
      if(yp >= 1.0-epsilon && yp <= 1.0+epsilon) this->_particles[atom]._setExistance(false);
      if(zp >= 1.0-epsilon && zp <= 1.0+epsilon) this->_particles[atom]._setExistance(false);
  }

}
void RawState::System::_createNeighboursList(double r_cutoff, int max_neighbours, bool normalize)
{
  this->_calculateBasisVectors();
	this->_wrapAtoms(normalize);
	this->_neighbours = new NeighbourList;	
	this->_neighbours->_setNumberOfAtoms(this->_n_atoms);
  this->_r_cutoff = r_cutoff;
  this->_neighbours->_r_cutoff = r_cutoff;
  this->_neighbours->_max_neighbours = max_neighbours;
	this->_neighbours->_prepareNeighboursList();
	this->_neighbours->_buildNeighboursList(this->_basis_vectors, this->_particles, this->_periodic);
}
bool RawState::System::_readNextLAMMPSTimestep(std::string filename)
{
	std::ifstream input_data;
	input_data.open(filename.c_str());

	int n_atoms = 0;
	int linecounter = 0;
	stringstream linestream;
	string line;

	while(linecounter < 9)
	{
		getline(input_data, line);
		if(linecounter == 3)
		{
			linestream.str(line);
			linestream >> n_atoms;
			this->_n_atoms = n_atoms;
			
			if(this->_particles != nullptr) delete [] this->_particles;

			this->_createParticlesTable();
			linestream.clear();
			linestream.str("");
		}
		if(linecounter == 4)
		{
			linestream.str(line);
			string keyword;
			for(int i = 0; i < 6; i++) linestream >> keyword;
			for(int i = 0; i < 3; i++)
			{
				linestream >> keyword;
				if(keyword.find("pp") == std::string::npos) this->_periodic[i] = false;
				if(keyword.find("pp") != std::string::npos) this->_periodic[i] = true;
			}

			linestream.str("");
			linestream.clear();
		}
		if(linecounter == 8)
		{
			bool toggle_velocity = false;
			bool toggle_force = false;

			if(line.find(" vx ") != std::string::npos) toggle_velocity = true;
			if(line.find(" fx ") != std::string::npos) toggle_force = true;
			if(line.find(" xs ") != std::string::npos || line.find(" ys ") != std::string::npos || line.find(" zs ") != std::string::npos) 
      {
        this->_normalize = false; 
      }

      for(int atom_i = 0; atom_i < this->_n_atoms; atom_i++)
      {
        this->_particles[atom_i]._createPositionsTable();
        
        if(toggle_velocity)
        {
          if(this->_particles[atom_i]._velocity != nullptr)
          {
            delete [] this->_particles[atom_i]._velocity;
          }
          this->_particles[atom_i]._createVelocitiesTable();	
          this->_toggle_velocity = true;
        }
        if(toggle_force)
        {
          if(this->_particles[atom_i]._force != nullptr)
          {
            delete [] this->_particles[atom_i]._force;
          }
          this->_particles[atom_i]._createForcesTable();	
          this->_toggle_force = true;
        }
			}
		}
		linecounter++;
	}
	
	int starting_position = (9 + n_atoms) *  _state_timestep;	

	linestream.str("");
	linestream.clear();
	input_data.clear();
	input_data.seekg(0);
	linecounter = 0;

	bool start_of_timestep = false;
  bool end_reading = false;

	while(getline(input_data,line))
	{
    if(end_reading) break;
		if(start_of_timestep)
		{
			if(linecounter == 1)
			{	
				linestream.str(line);
				int timestep_label;
				linestream >> timestep_label;
				this->_timestep_label = timestep_label; 
				linestream.str("");
				linestream.clear();
			}
			if(linecounter >= 5 && linecounter <= 7)
			{
				linestream.str(line);
				double column_entry;

				for(int i = 0; i < 3; i++)
				{
					if(linestream.eof())
					{
						this->_box_data[linecounter - 5][i] = 0.0;
					}
					if(!linestream.eof())
					{
						linestream >> column_entry;
						this->_box_data[linecounter - 5][i] = column_entry;
					}
				}

				linestream.str("");
				linestream.clear();
			}
			if(linecounter >= 9 && linecounter <= 9+this->_n_atoms-1)
			{
				int atom_type;
				linestream.str(line);
				
				linestream >> atom_type;
				linestream >> atom_type;
				this->_particles[linecounter-9]._type_id = atom_type;

				for(int i = 0; i < 3; i++)
					linestream >> this->_particles[linecounter-9]._position[i];
				if(this->_toggle_velocity == true)
				{
					for(int i = 0; i < 3; i++)
						linestream >> this->_particles[linecounter-9]._velocity[i];
				}
				if(this->_toggle_force == true)
				{
					for(int i = 0; i < 3; i++)
						linestream >> this->_particles[linecounter-9]._force[i];
				}

				linestream.str("");
				linestream.clear();
			}
			
		}
		if(!start_of_timestep)
		{
			if(linecounter == starting_position)
			{
				linecounter = 0;
				start_of_timestep = true;
			}
		}
		linecounter++;
    if(start_of_timestep) if(linecounter == 9 + this->_n_atoms) end_reading = true;
			
	}

	setStateTimestep(_state_timestep + _state_timestep_delta);
	input_data.close();
  return true;
}
void RawState::printParticlePositions(RawState::System *&input)
{
	int n_atoms = input->_n_atoms;
	for(int atom_i = 0; atom_i < n_atoms; atom_i++)
	{
		cout << input->_particles[atom_i]._type_id << " ";
		for(int dimension = 0; dimension < 3; dimension++)
			cout << input->_particles[atom_i]._position[dimension] << " ";
		cout << "\n";
	}
}
void RawState::printParticleRelativePositions(RawState::System *&input)
{
	int n_atoms = input->_n_atoms;
	for(int atom_i = 0; atom_i < n_atoms; atom_i++)
	{
		cout << input->_particles[atom_i]._type_id << " ";
		for(int dimension = 3; dimension < 6; dimension++)
			cout << input->_particles[atom_i]._position[dimension] << " ";
		cout << "\n";
	}
}
void RawStateGeometry::readPOSCAR(std::string filename, RawState::System *&input)
{
  ifstream file;

  string line;
  
  file.open(filename.c_str());

  getline(file,line);
  
  int n_atoms = std::stod(line);
  input->_setNAtoms(n_atoms);
  input->_createParticlesTable();

  for (int atom = 0; atom < n_atoms; ++atom)
    input->_particles[atom]._createPositionsTable();

  getline(file,line);

  string poscar_description = line;

  stringstream labelstream;
  string label;

  stringstream linestream;
  int linecounter = 0;

  bool read_coordinates = false;

  while(getline(file,line))
  {
    if(linecounter == 0) read_coordinates = true;
    if(linecounter == n_atoms) read_coordinates = false;
    if(read_coordinates)
    {
      linestream.str(line);
      linestream >> label;
      labelstream << label << " ";
      linestream >> input->_particles[linecounter]._position[0];
      linestream >> input->_particles[linecounter]._position[1];
      linestream >> input->_particles[linecounter]._position[2];
      input->_particles[linecounter]._setExistance(true);
    }
    linecounter++;
    linestream.str("");
    linestream.clear();
  }
  
  labelstream.clear();
  string labels = labelstream.str();

  int substitution_counter = 0;

  string key_label;
  while(labelstream >> key_label)
  {
    if(labels.find(key_label) != std::string::npos ) substitution_counter++;
    replaceSubstring(labels,key_label,to_string(substitution_counter));
  }

  labelstream.str(labels);
  labelstream.clear();

  int type_id;

  for (int atom = 0; atom < input->_n_atoms; ++atom)
  {
    labelstream >> type_id;
    input->_particles[atom]._setTypeId(type_id);
  }

  file.close();

  input->_createParseData();

  input->_parse_data->_filepath = filename; 
  input->_parse_data->_description = poscar_description;
  input->_parse_data->_n_atoms = n_atoms;
  input->_parse_data->_n_types = substitution_counter;
  input->_parse_data->_initializeStechiometry();

  double *maxima = new double [3];
  double *minima = new double [3];

  for (int dim = 0; dim < 3; dim++)
  {
   maxima[dim] = -DBL_MAX;
   minima[dim] = DBL_MAX;
  }

  for (int dim = 0; dim < 3; dim++)
  {
    for (int atom = 0; atom < input->_n_atoms; ++atom)
    {
      if(input->_particles[atom]._position[dim] < minima[dim]) 
        minima[dim] = input->_particles[atom]._position[dim];
      if(input->_particles[atom]._position[dim] > maxima[dim]) 
        maxima[dim] = input->_particles[atom]._position[dim];
    }
  }
  
  for (int atom = 0; atom < input->_n_atoms; ++atom)
  {
    for (int dim = 0; dim < 3; dim++)
    {
      input->_particles[atom]._position[dim] -= minima[dim];
      input->_particles[atom]._position[dim] += SLIGHT_SHIFT;
    }
  }

  for (int dim = 0; dim < 3; dim++)
  {
    for (int atom = 0; atom < input->_n_atoms; ++atom)
      input->_particles[atom]._position[dim + 3] = input->_particles[atom]._position[dim] / ( maxima[dim] - minima[dim] );
  }

  determineStechiometry(input);

  delete [] maxima;
  delete [] minima;
}
void RawStateGeometry::readXYZ(std::string filename, RawState::System *&input)
{
  ifstream file;

  string line;
  
  file.open(filename.c_str());

  getline(file,line);
  
  int n_atoms = std::stod(line);
  input->_setNAtoms(n_atoms);
  input->_createParticlesTable();

  for (int atom = 0; atom < n_atoms; ++atom)
    input->_particles[atom]._createPositionsTable();

  getline(file,line);

  string poscar_description = line;

  stringstream labelstream;
  string label;

  stringstream linestream;
  int linecounter = 0;

  bool read_coordinates = false;

  while(getline(file,line))
  {
    if(linecounter == 0) read_coordinates = true;
    if(linecounter == n_atoms) read_coordinates = false;
    if(read_coordinates)
    {
      linestream.str(line);
      linestream >> label;
      labelstream << label << " ";
      linestream >> input->_particles[linecounter]._position[0];
      linestream >> input->_particles[linecounter]._position[1];
      linestream >> input->_particles[linecounter]._position[2];
      input->_particles[linecounter]._setExistance(true);
    }
    linecounter++;
    linestream.str("");
    linestream.clear();
  }
  
  labelstream.clear();
  string labels = labelstream.str();

  int substitution_counter = 0;

  string key_label;
  while(labelstream >> key_label)
  {
    if(labels.find(key_label) != std::string::npos ) substitution_counter++;
    replaceSubstring(labels,key_label,to_string(substitution_counter));
  }

  labelstream.str(labels);
  labelstream.clear();

  int type_id;

  for (int atom = 0; atom < input->_n_atoms; ++atom)
  {
    labelstream >> type_id;
    input->_particles[atom]._setTypeId(type_id);
  }

  file.close();

  input->_createParseData();

  input->_parse_data->_filepath = filename; 
  input->_parse_data->_description = poscar_description;
  input->_parse_data->_n_atoms = n_atoms;
  input->_parse_data->_n_types = substitution_counter;
  input->_parse_data->_initializeStechiometry();

  double *maxima = new double [3];
  double *minima = new double [3];

  for (int dim = 0; dim < 3; dim++)
  {
   maxima[dim] = -DBL_MAX;
   minima[dim] = DBL_MAX;
  }

  for (int dim = 0; dim < 3; dim++)
  {
    for (int atom = 0; atom < input->_n_atoms; ++atom)
    {
      if(input->_particles[atom]._position[dim] < minima[dim]) 
        minima[dim] = input->_particles[atom]._position[dim];
      if(input->_particles[atom]._position[dim] > maxima[dim]) 
        maxima[dim] = input->_particles[atom]._position[dim];
    }
  }
  
  for (int atom = 0; atom < input->_n_atoms; ++atom)
  {
    for (int dim = 0; dim < 3; dim++)
    {
      input->_particles[atom]._position[dim] -= minima[dim]; 
      input->_particles[atom]._position[dim] += SLIGHT_SHIFT;
    }
  }

  for (int dim = 0; dim < 3; dim++)
  {
    for (int atom = 0; atom < input->_n_atoms; ++atom)
      input->_particles[atom]._position[dim + 3] = input->_particles[atom]._position[dim] / ( maxima[dim] - minima[dim] );
  }

  determineStechiometry(input);

  delete [] maxima;
  delete [] minima;
}

// Quick function to check if atom partial coordinate is in a given range

bool checkPosition(double cord, double min, double max)
{
  if( cord >= min && cord <= max ) return true;  
  else return false;
}
double determinePosition(double x, double y, double z)
{
  bool vertex = false;
  bool face = false;
  bool edge = false;
  bool volume = false;
  double position_error = 0.01;

  if( checkPosition(x,0.0,position_error) )
  {
    if( checkPosition(y,0.0,position_error) )
    {
      if( checkPosition(z,0.0,position_error) )                vertex = true;
      if( checkPosition(z,position_error,1.0-position_error) ) edge = true;
      if( checkPosition(z,1.0-position_error,2.0) )            vertex = true;
    }
    if( checkPosition(y,position_error,1.0-position_error) )
    {
      if( checkPosition(z,0.0,position_error) )                edge = true;
      if( checkPosition(z,position_error,1.0-position_error) ) face = true;
      if( checkPosition(z,1.0-position_error,2.0) )            edge = true;
    }
    if( checkPosition(y,1.0-position_error,2.0) )
    {
      if( checkPosition(z,0.0,position_error) )                vertex = true;
      if( checkPosition(z,position_error,1.0-position_error) ) edge = true;
      if( checkPosition(z,1.0-position_error,2.0) )            vertex = true;
    }
  }
  if( checkPosition(x,position_error,1.0-position_error) )
  {
    if( checkPosition(y,0.0,position_error) )
    {
      if( checkPosition(z,position_error,1.0-position_error) ) face = true;
      if( checkPosition(z,0.0,position_error) )                edge = true;
      if( checkPosition(z,1.0-position_error,2.0) )            edge = true;
    }
    if( checkPosition(y,position_error,1.0-position_error) )
    {
      if( checkPosition(z,0.0,position_error) )                face = true;
      if( checkPosition(z,position_error,1.0-position_error) ) volume = true;
      if( checkPosition(z,1.0-position_error,2.0) )            face = true;
    }
    if( checkPosition(y,1.0-position_error,2.0) ) 
    {
      if( checkPosition(z,0.0,position_error) )                edge = true;
      if( checkPosition(z,position_error,1.0-position_error) ) face = true;
      if( checkPosition(z,1.0-position_error,2.0) )            edge = true;
    }
  }
  if( checkPosition(x,1.0-position_error,2.0) )
  {
    if( checkPosition(y,0.0,position_error) )
    {
      if( checkPosition(z,0.0,position_error) )                vertex = true;
      if( checkPosition(z,position_error,1.0-position_error) ) edge = true;
      if( checkPosition(z,1.0-position_error,2.0) )            vertex = true;
    }
    if( checkPosition(y,position_error,1.0-position_error) )
    {
      if( checkPosition(z,0.0,position_error) )                edge = true;
      if( checkPosition(z,position_error,1.0-position_error) ) face = true;
      if( checkPosition(z,1.0-position_error,2.0) )            edge = true;
    }
    if( checkPosition(y,1.0-position_error,2.0) )
    {
      if( checkPosition(z,0.0,position_error) )                vertex = true;
      if( checkPosition(z,position_error,1.0-position_error) ) edge = true;
      if( checkPosition(z,1.0-position_error,2.0) )            vertex = true;
    }
  }

  if(vertex) return 0.125;
  if(face)   return 0.5;
  if(edge)   return 0.25;
  if(volume) return 1.0;
}
void RawStateGeometry::determineStechiometry(RawState::System *&input)
{
  int n_atoms = input->_n_atoms;

  double *occupancy_counters = new double [input->_parse_data->_n_types];

  for (int type = 0; type < input->_parse_data->_n_types; ++type)
    occupancy_counters[type] = 0.0; 

  for(int atom = 0; atom < n_atoms; atom++)
  {
    double x_i = input->_particles[atom]._position[3];
    double y_i = input->_particles[atom]._position[4];
    double z_i = input->_particles[atom]._position[5];
    int type = input->_particles[atom]._type_id - 1;

    double occupancy = determinePosition(x_i,y_i,z_i);
    occupancy_counters[type] += occupancy;

  }

  for (int type = 0; type < input->_parse_data->_n_types; ++type)
    input->_parse_data->_stechiometry[type] = round(occupancy_counters[type]);

  int common_divisor = input->_parse_data->_stechiometry[0];
  for(int type = 0; type < input->_parse_data->_n_types; type++)
    common_divisor = __gcd(common_divisor,input->_parse_data->_stechiometry[type]);
  for(int type = 0; type < input->_parse_data->_n_types; type++)
    input->_parse_data->_stechiometry[type] /= common_divisor;

  stringstream descstream;
  string alphabet = "abcdefghijklmnopqrstuvwxyz";

  char letter;

  for(int type = 0; type < input->_parse_data->_n_types; type++)
  {
    letter = alphabet[type];
    char upperletter = toupper(letter);
    descstream << upperletter << input->_parse_data->_stechiometry[type];
  }

  input->_parse_data->_stechiometry_desc = descstream.str();
  delete [] occupancy_counters;
}
void RawStateGeometry::assignElements(RawState::System *&input, std::string elements)
{
  input->_parse_data->_type_labels = elements; 
}

// Helper function to permute array (here ints) using bits STL

void permuteAtomTypes(int *input, int n_elements, int **output)
{
  sort(input,input+n_elements);
  
  int permutation = 0;

  do
  {
    for (int i = 0; i < n_elements; ++i)
      output[permutation][i] = input[i];     

    permutation++;
  
  }while(std::next_permutation(input, input+n_elements));
}
int RawStateGeometry::createHypotheticalStructures(RawState::System *&input, RawState::System **&output)
{
  double *atomic_radii = new double [input->_parse_data->_n_types];
  double *atomic_masses = new double [input->_parse_data->_n_types];

  stringstream labels;
  labels.str(input->_parse_data->_type_labels);

  string label;
  int type_counter = 0;

  while(labels >> label)
  {
    atomic_masses[type_counter] = RawStateLibrary::atomicMassList(label);
    atomic_radii[type_counter]  = RawStateLibrary::atomicRadiusList(label);
    type_counter++;
  }
  
  int n_atoms = input->_n_atoms;

  double r_min = DBL_MAX;
  int *min_indices = new int [2];

  double x_i, y_i, z_i;
  double x_j, y_j, z_j;
  double dx,dy,dz,dr;

  for (int atom_i = 0; atom_i < n_atoms; ++atom_i)
  {
    x_i = input->_particles[atom_i]._position[0];
    y_i = input->_particles[atom_i]._position[1];
    z_i = input->_particles[atom_i]._position[2];

    for (int atom_j = atom_i + 1; atom_j < n_atoms; ++atom_j)
    {
      x_j = input->_particles[atom_j]._position[0];
      y_j = input->_particles[atom_j]._position[1];
      z_j = input->_particles[atom_j]._position[2];

      dx = x_i - x_j;
      dy = y_i - y_j;
      dz = z_i - z_j;
      
      dr = dx * dx + dy * dy + dz * dz;
      dr = sqrt(dr);

      if( dr < r_min )
      {
        r_min = dr;
        min_indices[0] = atom_i;
        min_indices[1] = atom_j;
      }
    }
  }
  
  double *maxima = new double [3];
  double *minima = new double [3];

  for (int dim = 0; dim < 3; dim++)
  {
    maxima[dim] = -DBL_MAX;
    minima[dim] = DBL_MAX;
  }

  for (int dim = 0; dim < 3; dim++)
  {
    for (int atom = 0; atom < input->_n_atoms; ++atom)
    {
      if(input->_particles[atom]._position[dim] < minima[dim]) 
        minima[dim] = input->_particles[atom]._position[dim];
      if(input->_particles[atom]._position[dim] > maxima[dim]) 
        maxima[dim] = input->_particles[atom]._position[dim];
    }
  }

  int min_type_i = input->_particles[min_indices[0]]._type_id;
  int min_type_j = input->_particles[min_indices[1]]._type_id;
  
  double *box_lenghts = new double [3];

  for(int dim = 0; dim < 3; dim++)
    box_lenghts[dim] = maxima[dim] - minima[dim];
  
  double *proportions = new double [3];
  
  for(int dim = 0; dim < 3; dim++)
    proportions[dim] = box_lenghts[dim] / r_min;

  input->_enablePeriodicity();

  input->_box_data[0][0] = 0.0;
  input->_box_data[0][1] = maxima[0];
  input->_box_data[1][0] = 0.0;
  input->_box_data[1][1] = maxima[1];
  input->_box_data[2][0] = 0.0;
  input->_box_data[2][1] = maxima[2];

  input->_box_data[0][2] = 0.0;
  input->_box_data[1][2] = 0.0;
  input->_box_data[2][2] = 0.0;

  input->_calculateBasisVectors();

  int present_atoms = 0;
  int original_atoms = n_atoms;

  for (int atom = 0; atom < n_atoms; ++atom)
    if(input->_particles[atom]._is_present == true) present_atoms++;

  stringstream ss_type_labels;
  ss_type_labels.str(input->_parse_data->_type_labels);

  int *type_array = new int [type_counter];
  for (int type = 0; type < type_counter; type++) 
    type_array[type] = type + 1;

  int n_structures;
  
  bool even_stechiometry = true;

  for (int type = 0; type < type_counter - 1; ++type)
    if(input->_parse_data->_stechiometry[type] 
      != input->_parse_data->_stechiometry[type+1]) even_stechiometry = false;
  
  if(even_stechiometry) n_structures = 1; 
  if(!even_stechiometry) n_structures = factorial(input->_parse_data->_n_types);
  
  int **permutations = new int *[n_structures];
  for (int i = 0; i < n_structures; i++)
    permutations[i] = new int [type_counter];

  if(!even_stechiometry) permuteAtomTypes(type_array, type_counter, permutations);
  if(even_stechiometry)
  {
    for (int type = 0; type < type_counter; ++type)
      permutations[0][type] = type_array[type];
  }

  output = new RawState::System *[n_structures];
  for (int str = 0; str < n_structures; ++str)
    output[str] = new RawState::System;
  
  double r_a, r_b, r_e;

  for (int structure = 0; structure < n_structures; ++structure)
  {
    output[structure]->_misc_description = input->_parse_data->_stechiometry_desc;
    output[structure]->_n_atoms = present_atoms;
    output[structure]->_createParticlesTableWithPositions();

    r_a = atomic_radii[permutations[structure][min_type_i-1]-1];
    r_b = atomic_radii[permutations[structure][min_type_j-1]-1];
    r_e = r_a + r_b;

    int atom_counter = 0;

    for (int atom = 0; atom < original_atoms; ++atom)
    {
      if(input->_particles[atom]._is_present == true)
      {
        for (int dim = 3; dim < 6; ++dim)
        {
          double coordinate = input->_particles[atom]._position[dim];
          output[structure]->_particles[atom_counter]._position[dim] = coordinate; 
        }
        int type = input->_particles[atom]._type_id;
        int permutated_type = permutations[structure][type-1];

        double mass = atomic_masses[permutated_type-1];

        output[structure]->_particles[atom_counter]._setExistance(true);
        output[structure]->_particles[atom_counter]._setTypeId(type);
        output[structure]->_particles[atom_counter]._setMass(mass);
        atom_counter++;
      }
    }

    // Calculating box dimensions

    double x_hi,y_hi,z_hi;
    
    x_hi = r_e * proportions[0];
    y_hi = r_e * proportions[1];
    z_hi = r_e * proportions[2];

    for (int dim = 0; dim < 3; ++dim)
    {
      output[structure]->_box_data[dim][0] = 0.0;
      output[structure]->_box_data[dim][2] = 0.0;
    }
    output[structure]->_box_data[0][1] = x_hi;
    output[structure]->_box_data[1][1] = y_hi;
    output[structure]->_box_data[2][1] = z_hi;
    output[structure]->_calculateBasisVectors();
    
    for (int atom = 0; atom < atom_counter; ++atom)
    {
      double x_coordinate = output[structure]->_particles[atom]._position[3];
      double y_coordinate = output[structure]->_particles[atom]._position[4];
      double z_coordinate = output[structure]->_particles[atom]._position[5];

      output[structure]->_particles[atom]._position[0] = x_coordinate * x_hi; 
      output[structure]->_particles[atom]._position[1] = y_coordinate * y_hi; 
      output[structure]->_particles[atom]._position[2] = z_coordinate * z_hi; 
    }

    stringstream ss_tag;
    for (int type = 0; type < type_counter; ++type) 
      ss_tag << permutations[structure][type] << input->_parse_data->_stechiometry[type] << " ";
    output[structure]->_misc_description = ss_tag.str();
  }

  
  for (int i = 0; i < n_structures; ++i)
    delete [] permutations[i];
  delete [] permutations;

  delete [] type_array;
  delete [] atomic_radii;
  delete [] atomic_masses;
  delete [] min_indices;
  delete [] minima;
  delete [] maxima;
  delete [] box_lenghts;
  delete [] proportions;

  return n_structures;
}
int RawStateLAMMPSTools::getNumberOfReadings(std::string filename)
{
	ifstream file;

	file.open(filename.c_str());	
	int tmp_linecounter = 0;
	string tmp_line;

	while(tmp_linecounter !=4)
	{
		getline(file, tmp_line);
		tmp_linecounter++;
	}

	int tmp_n_atoms = std::atoi(tmp_line.c_str());

	file.clear();
	file.seekg(0);
	tmp_linecounter = 0;

	while(getline(file,tmp_line))
	{
		tmp_linecounter++;
	}
  file.close();

  int total_timesteps = tmp_linecounter / (tmp_n_atoms + 9);
  int read_timesteps = ( ( total_timesteps - _state_timestep ) / _state_timestep_delta ) ;

	return read_timesteps;

}

// RawStateAnalysis NS



// RawStateLammpsTools NS

void RawStateLAMMPSTools::writeTimestepDataToFile(std::string filename, RawState::System *&input)
{
  int n_atoms = input->_n_atoms;
  int actual_atoms = 0;
  for (int atom_i = 0; atom_i < n_atoms; ++atom_i)
    if(input->_particles[atom_i]._is_present) actual_atoms++;
    
  ofstream output;

  output.open(filename.c_str());

  output << "RawState Output Data File\n\n";
  output << actual_atoms << " atoms\n";
  output << "0 bonds\n0 angles\n0 dihedrals\n0 impropers\n\n";

  vector<int> atom_types;
  for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    atom_types.push_back(input->_particles[atom_i]._type_id);
  std::set<int> counter;
  for (int i = 0; i < n_atoms; i++)
    counter.insert(atom_types[i]);

  output << counter.size() << " atom types\n\n";
  output << std::setprecision(12) << input->_box_data[0][0] << " " << input->_box_data[0][1] << " xlo xhi\n";
  output << std::setprecision(12) << input->_box_data[1][0] << " " << input->_box_data[1][1] << " ylo yhi\n";
  output << std::setprecision(12) << input->_box_data[2][0] << " " << input->_box_data[2][1] << " zlo zhi\n";
  output << std::setprecision(12) << input->_box_data[0][2] << " " << input->_box_data[1][2] << " " << input->_box_data[2][2] << " xy xz yz\n\n";
  output << std::setprecision(12) << "Masses\n\n";

  for (int type = 0; type < counter.size(); ++type)
  {
    for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    {
      if(input->_particles[atom_i]._type_id == type + 1)
      {
        output << type + 1 << " " << input->_particles[atom_i]._mass << "\n";
        break;
      }
    }
  }

  output << "\nAtoms\n\n";
  for(int atom_i = 0; atom_i < n_atoms; atom_i++)
  {
    if(input->_particles[atom_i]._is_present)
    {
      output << atom_i+1 << " " << input->_particles[atom_i]._type_id << " ";
      for (int dimension = 0; dimension < 3; ++dimension)
        output << input->_particles[atom_i]._position[dimension] << " ";  
      output << "\n";
    }
  }

  output.close();
  output.clear();
}
void RawStateLAMMPSTools::writeTimestepDataToFile(std::string filename, std::string header, RawState::System *&input)
{
  int n_atoms = input->_n_atoms;
  int actual_atoms = 0;
  for (int atom_i = 0; atom_i < n_atoms; ++atom_i)
    if(input->_particles[atom_i]._is_present) actual_atoms++;
    
  ofstream output;

  output.open(filename.c_str());
  
  output << header;
  output << "\n\n";
  output << actual_atoms << " atoms\n";
  output << "0 bonds\n0 angles\n0 dihedrals\n0 impropers\n\n";

  vector<int> atom_types;
  for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    atom_types.push_back(input->_particles[atom_i]._type_id);
  std::set<int> counter;
  for (int i = 0; i < n_atoms; i++)
    counter.insert(atom_types[i]);

  output << counter.size() << " atom types\n\n";
  output << std::setprecision(12) << input->_box_data[0][0] << " " << input->_box_data[0][1] << " xlo xhi\n";
  output << std::setprecision(12) << input->_box_data[1][0] << " " << input->_box_data[1][1] << " ylo yhi\n";
  output << std::setprecision(12) << input->_box_data[2][0] << " " << input->_box_data[2][1] << " zlo zhi\n";
  output << std::setprecision(12) << input->_box_data[0][2] << " " << input->_box_data[1][2] << " " << input->_box_data[2][2] << " xy xz yz\n\n";
  output << std::setprecision(12) << "Masses\n\n";

  for (int type = 0; type < counter.size(); ++type)
  {
    for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    {
      if(input->_particles[atom_i]._type_id == type + 1)
      {
        output << type + 1 << " " << input->_particles[atom_i]._mass << "\n";
        break;
      }
    }
  }

  output << "\nAtoms\n\n";
  for(int atom_i = 0; atom_i < n_atoms; atom_i++)
  {
    if(input->_particles[atom_i]._is_present == 1)
    {
      output << atom_i+1 << " " << input->_particles[atom_i]._type_id << " ";
      for (int dimension = 0; dimension < 3; ++dimension)
        output << input->_particles[atom_i]._position[dimension] << " ";  
      output << "\n";
    }
  }

  output.close();
  output.clear();
}
void RawStateLAMMPSTools::writeTimestepDataToFile(std::string filename, RawState::System *&input, std::string write_style)
{
  int n_atoms = input->_n_atoms;
  int actual_atoms = 0;
  for (int atom_i = 0; atom_i < n_atoms; ++atom_i)
    if(input->_particles[atom_i]._is_present) actual_atoms++;
    
  ofstream output;

  output.open(filename.c_str());

  output << "RawState Output Data File\n\n";
  output << actual_atoms << " atoms\n";
  output << "0 bonds\n0 angles\n0 dihedrals\n0 impropers\n\n";

  vector<int> atom_types;
  for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    atom_types.push_back(input->_particles[atom_i]._type_id);
  std::set<int> counter;
  for (int i = 0; i < n_atoms; i++)
    counter.insert(atom_types[i]);

  output << counter.size() << " atom types\n\n";
  output << std::setprecision(12) << input->_box_data[0][0] << " " << input->_box_data[0][1] << " xlo xhi\n";
  output << std::setprecision(12) << input->_box_data[1][0] << " " << input->_box_data[1][1] << " ylo yhi\n";
  output << std::setprecision(12) << input->_box_data[2][0] << " " << input->_box_data[2][1] << " zlo zhi\n";
  output << std::setprecision(12) << input->_box_data[0][2] << " " << input->_box_data[1][2] << " " << input->_box_data[2][2] << " xy xz yz\n\n";
  output << std::setprecision(12) << "Masses\n\n";

  for (int type = 0; type < counter.size(); ++type)
  {
    for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    {
      if(input->_particles[atom_i]._type_id == type + 1)
      {
        output << type + 1 << " " << input->_particles[atom_i]._mass << "\n";
        break;
      }
    }
  }

  output << "\nAtoms\n\n";
  for(int atom_i = 0; atom_i < n_atoms; atom_i++)
  {
    if(input->_particles[atom_i]._is_present == 1)
    {
      output << atom_i+1 << " " << input->_particles[atom_i]._type_id << " ";
      for (int dimension = 0; dimension < 3; ++dimension)
        output << input->_particles[atom_i]._position[dimension] << " ";  
      output << "\n";
    }
  }

  if(write_style.find_first_of("vel") != std::string::npos)
  {
    output << "\nVelocities\n\n";
    for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    {
      if(input->_particles[atom_i]._is_present == 1)
      {
        output << atom_i+1 << " ";
        for (int dimension = 0; dimension < 3; ++dimension)
          output << input->_particles[atom_i]._velocity[dimension] << " ";  
        output << "\n";
      }
    }
  }
  if(write_style.find_first_of("for") != std::string::npos)
  {
    output << "\nForces\n\n";
    for(int atom_i = 0; atom_i < n_atoms; atom_i++)
    {
      if(input->_particles[atom_i]._is_present == 1)
      {
        output << atom_i+1 << " " << input->_particles[atom_i]._type_id << " ";
        for (int dimension = 0; dimension < 3; ++dimension)
          output << input->_particles[atom_i]._force[dimension] << " ";  
        output << "\n";
      }
    }
  }

  output.close();
  output.clear();
}
double RawStateLibrary::atomicRadiusList(std::string atomtype)
{
  if(atomtype == "He" ) {return 0.49;}
  if(atomtype == "Ne" ) {return 0.51;}
  if(atomtype == "F" ) {return 0.57;}
  if(atomtype == "O" ) {return 0.65;}
  if(atomtype == "N" ) {return 0.75;}
  if(atomtype == "H" ) {return 0.79;}
  if(atomtype == "Ar" ) {return 0.88;}
  if(atomtype == "C" ) {return 0.91;}
  if(atomtype == "Cl" ) {return 0.97;}
  if(atomtype == "Kr" ) {return 1.03;}
  if(atomtype == "S" ) {return 1.09;}
  if(atomtype == "Br" ) {return 1.12;}
  if(atomtype == "B" ) {return 1.17;}
  if(atomtype == "Se" ) {return 1.22;}
  if(atomtype == "P" ) {return 1.23;}
  if(atomtype == "Xe" ) {return 1.24;}
  if(atomtype == "I" ) {return 1.32;}
  if(atomtype == "As" ) {return 1.33;}
  if(atomtype == "Rn" ) {return 1.34;}
  if(atomtype == "Be" ) {return 1.4;}
  if(atomtype == "Te" ) {return 1.42;}
  if(atomtype == "At" ) {return 1.43;}
  if(atomtype == "Si" ) {return 1.46;}
  if(atomtype == "Ge" ) {return 1.52;}
  if(atomtype == "Po" ) {return 1.53;}
  if(atomtype == "Sb" ) {return 1.53;}
  if(atomtype == "Zn" ) {return 1.53;}
  if(atomtype == "Cu" ) {return 1.57;}
  if(atomtype == "Ni" ) {return 1.62;}
  if(atomtype == "Bi" ) {return 1.63;}
  if(atomtype == "Co" ) {return 1.67;}
  if(atomtype == "Cd" ) {return 1.71;}
  if(atomtype == "Sn" ) {return 1.72;}
  if(atomtype == "Fe" ) {return 1.72;}
  if(atomtype == "Mg" ) {return 1.72;}
  if(atomtype == "Ag" ) {return 1.75;}
  if(atomtype == "U" ) {return 1.75;}
  if(atomtype == "Hg" ) {return 1.76;}
  if(atomtype == "Pd" ) {return 1.79;}
  if(atomtype == "Mn" ) {return 1.79;}
  if(atomtype == "Au" ) {return 1.79;}
  if(atomtype == "Ga" ) {return 1.81;}
  if(atomtype == "Pb" ) {return 1.81;}
  if(atomtype == "Al" ) {return 1.82;}
  if(atomtype == "Pt" ) {return 1.83;}
  if(atomtype == "Rh" ) {return 1.83;}
  if(atomtype == "Cr" ) {return 1.85;}
  if(atomtype == "Ir" ) {return 1.87;}
  if(atomtype == "Ac" ) {return 1.88;}
  if(atomtype == "Ru" ) {return 1.89;}
  if(atomtype == "V" ) {return 1.92;}
  if(atomtype == "Os" ) {return 1.92;}
  if(atomtype == "Tc" ) {return 1.95;}
  if(atomtype == "Re" ) {return 1.97;}
  if(atomtype == "In" ) {return 2.0;}
  if(atomtype == "Ti" ) {return 2.0;}
  if(atomtype == "Mo" ) {return 2.01;}
  if(atomtype == "W" ) {return 2.02;}
  if(atomtype == "Li" ) {return 2.05;}
  if(atomtype == "Nb" ) {return 2.08;}
  if(atomtype == "Tl" ) {return 2.08;}
  if(atomtype == "Sc" ) {return 2.09;}
  if(atomtype == "Ta" ) {return 2.09;}
  if(atomtype == "Hf" ) {return 2.16;}
  if(atomtype == "Zr" ) {return 2.16;}
  if(atomtype == "Na" ) {return 2.23;}
  if(atomtype == "Ca" ) {return 2.23;}
  if(atomtype == "Lu" ) {return 2.25;}
  if(atomtype == "Y" ) {return 2.27;}
  if(atomtype == "Yb" ) {return 2.4;}
  if(atomtype == "Tm" ) {return 2.42;}
  if(atomtype == "Sr" ) {return 2.45;}
  if(atomtype == "Er" ) {return 2.45;}
  if(atomtype == "Ho" ) {return 2.47;}
  if(atomtype == "Dy" ) {return 2.49;}
  if(atomtype == "Tb" ) {return 2.51;}
  if(atomtype == "Gd" ) {return 2.54;}
  if(atomtype == "Eu" ) {return 2.56;}
  if(atomtype == "Sm" ) {return 2.59;}
  if(atomtype == "Pm" ) {return 2.62;}
  if(atomtype == "Nd" ) {return 2.64;}
  if(atomtype == "Pr" ) {return 2.67;}
  if(atomtype == "Ce" ) {return 2.7;}
  if(atomtype == "La" ) {return 2.74;}
  if(atomtype == "K" ) {return 2.77;}
  if(atomtype == "Ba" ) {return 2.78;}
  if(atomtype == "Rb" ) {return 2.98;}
  if(atomtype == "Cs" ) {return 3.34;}

  std::cout << "Incorrect element!\n"; return 0;

}
double RawStateLibrary::atomicMassList(std::string atomtype)
{
  if(atomtype == "H"){return 1.00794;}
  if(atomtype == "He"){return 4.002602;}
  if(atomtype == "Li"){return 6.941;}
  if(atomtype == "Be"){return 9.012182;}
  if(atomtype == "B"){return 10.811;}
  if(atomtype == "C"){return 12.0107;}
  if(atomtype == "N"){return 14.0067;}
  if(atomtype == "O"){return 15.9994;}
  if(atomtype == "F"){return 18.9984032;}
  if(atomtype == "Ne"){return 20.1797;}
  if(atomtype == "Na"){return 22.98976928;}
  if(atomtype == "Mg"){return 24.3050;}
  if(atomtype == "Al"){return 26.9815386;}
  if(atomtype == "Si"){return 28.0855;}
  if(atomtype == "P"){return 30.973762;}
  if(atomtype == "S"){return 32.065;}
  if(atomtype == "Cl"){return 35.453;}
  if(atomtype == "Ar"){return 39.948;}
  if(atomtype == "K"){return 39.0983;}
  if(atomtype == "Ca"){return 40.078;}
  if(atomtype == "Sc"){return 44.955912;}
  if(atomtype == "Ti"){return 47.867;}
  if(atomtype == "V"){return 50.9415;}
  if(atomtype == "Cr"){return 51.9961;}
  if(atomtype == "Mn"){return 54.938045;}
  if(atomtype == "Fe"){return 55.845;}
  if(atomtype == "Co"){return 58.933195;}
  if(atomtype == "Ni"){return 58.6934;}
  if(atomtype == "Cu"){return 63.546;}
  if(atomtype == "Zn"){return 65.39;}
  if(atomtype == "Ga"){return 69.723;}
  if(atomtype == "Ge"){return 72.64;}
  if(atomtype == "As"){return 74.92160;}
  if(atomtype == "Se"){return 78.96;}
  if(atomtype == "Br"){return 79.904;}
  if(atomtype == "Kr"){return 83.798;}
  if(atomtype == "Rb"){return 85.4678;}
  if(atomtype == "Sr"){return 87.62;}
  if(atomtype == "Y"){return 88.90585;}
  if(atomtype == "Zr"){return 91.224;}
  if(atomtype == "Nb"){return 92.906;}
  if(atomtype == "Mo"){return 95.94;}
  if(atomtype == "Tc"){return 97.9072;}
  if(atomtype == "Ru"){return 101.07;}
  if(atomtype == "Rh"){return 102.905;}
  if(atomtype == "Pd"){return 106.42;}
  if(atomtype == "Ag"){return 107.8682;}
  if(atomtype == "Cd"){return 112.411;}
  if(atomtype == "In"){return 114.818;}
  if(atomtype == "Sn"){return 118.710;}
  if(atomtype == "Sb"){return 121.760;}
  if(atomtype == "Te"){return 127.60;}
  if(atomtype == "I"){return 126.904;}
  if(atomtype == "Xe"){return 131.293;}
  if(atomtype == "Cs"){return 132.9054519;}
  if(atomtype == "Ba"){return 137.327;}
  if(atomtype == "La"){return 138.90547;}
  if(atomtype == "Ce"){return 140.116;}
  if(atomtype == "Pr"){return 140.90765;}
  if(atomtype == "Nd"){return 144.242;}
  if(atomtype == "Pm"){return 144.9127;}
  if(atomtype == "Sm"){return 150.36;}
  if(atomtype == "Eu"){return 151.964;}
  if(atomtype == "Gd"){return 157.25;}
  if(atomtype == "Tb"){return 158.92535;}
  if(atomtype == "Dy"){return 162.500;}
  if(atomtype == "Ho"){return 164.930;}
  if(atomtype == "Er"){return 167.259;}
  if(atomtype == "Tm"){return 168.93421;}
  if(atomtype == "Yb"){return 173.04;}
  if(atomtype == "Lu"){return 174.967;}
  if(atomtype == "Hf"){return 178.49;}
  if(atomtype == "Ta"){return 180.94788;}
  if(atomtype == "W"){return 183.84;}
  if(atomtype == "Re"){return 186.207;}
  if(atomtype == "Os"){return 190.23;}
  if(atomtype == "Ir"){return 192.217;}
  if(atomtype == "Pt"){return 195.084;}
  if(atomtype == "Au"){return 196.966569;}
  if(atomtype == "Hg"){return 200.59;}
  if(atomtype == "Tl"){return 204.3833;}
  if(atomtype == "Pb"){return 207.2;}
  if(atomtype == "Bi"){return 208.98040;}
  if(atomtype == "Po"){return 208.9824;}
  if(atomtype == "At"){return 209.9871;}
  if(atomtype == "Rn"){return 222.0176;}
  if(atomtype == "Fr"){return 223.0197;}
  if(atomtype == "Ra"){return 226.0254;}
  if(atomtype == "Ac"){return 227.0277;}
  if(atomtype == "Th"){return 232.03806;}
  if(atomtype == "Pa"){return 231.03588;}
  if(atomtype == "U"){return 238.02891;}
  if(atomtype == "Np"){return 237.0482;}
  if(atomtype == "Pu"){return 244.0642;}
  if(atomtype == "Am"){return 243.0614;}
  if(atomtype == "Cm"){return 247.0704;}
  if(atomtype == "Bk"){return 247.0703;}
  if(atomtype == "Cf"){return 251.0796;}
  if(atomtype == "Es"){return 252.0830;}
  if(atomtype == "Fm"){return 257.0951;}
  if(atomtype == "Md"){return 258.0984;}
  if(atomtype == "No"){return 259.1010;}
  if(atomtype == "Lr"){return 262.1097;}
  if(atomtype == "Rf"){return 261.1088;}
  if(atomtype == "Db"){return 262;}
  if(atomtype == "Sg"){return 266;}
  if(atomtype == "Bh"){return 264;}
  if(atomtype == "Hs"){return 277;}
  if(atomtype == "Mt"){return 268;}
  if(atomtype == "Ds"){return 271;}
  if(atomtype == "Rg"){return 272;}
}

